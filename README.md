
**NOTE:** To see the repository as it was when then paper was published, please switch to branch published_version

Code for radiomics repeatability analysis.

analyse_radiomics_functions.py
  - Module containing functions used in analysing radiomics output (called from run_analyse_radiomics_output.py).
  - Contains code for calculating correlations between features, and calculating, comparing, and plotting repeatability metrics.

feat_class_index_dictionary.npy
  - Dictionary linking feature indices to their respective classes.

feature_names.txt
  - List of radiomic feature names.

figfuns.py
  - Module containing various functions for manipulating matplotlib figures.

helpers.py
  - Module containing various functions used in radiomics analysis (mainly file reading/writing).

icc_heatmap.py
  - Function for plotting ICC values from different images/normalisations as a heatmap.

Params_normalise_true.yaml / Params_normalise_false.yaml
  - YAML files specifying parameters used in feature extraction.

run_analyse_radiomics_output.py
  - Main function for analysing features output from radiomics   analysis.
  - Performs normality tests, Box-Cox transformations, ICC and RC calculations, and generates associated plots.

run_radiomics.py
  - Main function for extracting radiomics features.
  - For all relevant datasets, the specified images and their associated ROIs are loaded, and radiomics features are extracted according the specified YAML file.
  - Output saved as a csv file for each ROI.
