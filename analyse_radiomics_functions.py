#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Module containing functions used in analysing radiomics output.

Created on Wed Feb 5 09:15:46 2020

@author: damienmchugh
"""
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import stats

import figfuns

def calc_correlations(feature_array, feature_names, feature_class_idx_dict, 
                 feature_of_interest, save_path = None):
    """Function for analysing correlations between radiomics features.
    
    Inputs:
        feature_array - m x n list of m observations of n features
        feature_names - list of feature names
        feature_class_idx_dict - dictionary of indices for each feature class
        feature_of_interest - string specifying one feature, whose correlation
                              with all other features is plotted
                              e.g. feature_of_interest = 'shape_MeshVolume'
        save_path - optional argument of path to save figures if needed
    Outputs:
        corr_mat - Spearman's correlation matrix for all features
        pval_mat - p-value matrix for correlations
        corr_feat - vector of correlation coefficients for feature of interest
        pval_feat - vector of p-values for feature of interest
    """
    # Check inputs
    assert len(feature_array[0]) == len(feature_names), \
        "!!! number of features in array does not match number of names"
    
    feature_array = np.array(feature_array)
    corr_mat, pval_mat = stats.spearmanr(feature_array)
    
    fig, ax = plt.subplots()
    im = ax.imshow(corr_mat)
    im.set_clim(-1, 1)
    cbar = ax.figure.colorbar(im, ax=ax, format='% .2f')
    plt.show()
    plt.title(r"Spearman's $\rho$")
    if save_path == None:
        pass
    else:
        plt.savefig(os.path.join(save_path,'spearmans_rho_matrix'))
    
    fig, ax = plt.subplots()
    im = ax.imshow(pval_mat)
    im.set_clim(0, 1)
    cbar = ax.figure.colorbar(im, ax=ax, format='% .2f')
    plt.show()
    plt.title('p-value')
    if save_path == None:
        pass
    else:
        plt.savefig(os.path.join(save_path,'pval_matrix'))
    
    # Calculate rho and p-values for specified feature
    f_ind = feature_names.index(feature_of_interest)    
   
    corr_feat = corr_mat[f_ind,:]
    pval_feat = pval_mat[f_ind,:]
        
    # Plot rho and pval, coloured by feature class
    col_list = plt.cm.Set1(np.linspace(0, 1, len(feature_class_idx_dict.keys())))
    plt.figure()
    for idx, k in enumerate(feature_class_idx_dict):
        inds = feature_class_idx_dict[k]
        vals = [corr_feat[i] for i in inds]
        plt.plot(inds, vals, 'x', c = col_list[idx,:], label = k)
        plt.ylabel(r"Spearman's $\rho$")
        plt.xlabel('Feature index')
    plt.ylim(-1, 1)    
    plt.legend()
    plt.title(feature_of_interest)
    if save_path == None:
        pass
    else:
        fig_file = 'spearmans_rho_feat_class_' + feature_of_interest
        plt.savefig(os.path.join(save_path, fig_file))
    
    plt.figure()
    for idx, k in enumerate(feature_class_idx_dict):
        inds = feature_class_idx_dict[k]
        vals = [pval_feat[i] for i in inds]
        plt.plot(inds, vals, 'x', c = col_list[idx,:], label = k)
        plt.ylabel('p-value')
        plt.xlabel('Feature index')
    plt.ylim(0, 1) 
    plt.legend()
    plt.title(feature_of_interest)
    if save_path == None:
        pass
    else:
        fig_file = 'pval_feat_class_' + feature_of_interest
        plt.savefig(os.path.join(save_path, fig_file))
    
    # Return rho and pval for all features, and for specified feature
    return corr_mat, pval_mat, corr_feat, pval_feat

#-----------------------------------------------------------------------------            
def icc_depreciated(repeat_measurements, icc_type):
    """***********************************************************************
    **************************************************************************
    13/04/20 - this function is no longer used, but kept for reference.
             - replaced with icc function which also calculates confidence
               intervals.
    **************************************************************************
    **************************************************************************
                  
    Calculates intraclass correlation for pair-wise measurements.
    
    Taken from:
    https://github.com/cosanlab/nltools/blob/master/nltools/data/brain_data.py
    Currently only calculates Eqn 3 in Table 3 in Koo and Li, 2016
      - Equation holds for two-way random effects, absolute agreement, 
        single rater/measurement ICC, and for two-way mixed effects, 
        absolute agreement, single rater/measurement ICC
    
    Inputs:
        repeat_measurement - n x 2 array of pair-wise measurements
        icc_type - type of ICC to calculate
    Output:
        icc - intraclass correlation coefficient for icc_type model
    """
    Y = repeat_measurements
    [n, k] = Y.shape
    
    # Degrees of Freedom
    dfc = k - 1
    dfe = (n - 1) * (k-1)
    dfr = n - 1
    
    # Sum Square Total
    mean_Y = np.mean(Y)
    SST = ((Y - mean_Y) ** 2).sum()
    
    # create the design matrix for the different levels
    x = np.kron(np.eye(k), np.ones((n, 1)))  # sessions
    x0 = np.tile(np.eye(n), (k, 1))  # subjects
    X = np.hstack([x, x0])
    
    # Sum Square Error
    predicted_Y = np.dot(np.dot(np.dot(X, np.linalg.pinv(np.dot(X.T, X))),
                                X.T), Y.flatten('F'))
    residuals = Y.flatten('F') - predicted_Y
    SSE = (residuals ** 2).sum()
    
    MSE = SSE / dfe
    
    # Sum square column effect - between colums
    SSC = ((np.mean(Y, 0) - mean_Y) ** 2).sum() * n
    MSC = SSC / dfc / n
    
    # Sum Square subject effect - between rows/subjects
    SSR = SST - SSC - SSE
    MSR = SSR / dfr
    
    # Calculate ICC
    if icc_type == 'icc_2_1':
        icc = (MSR - MSE) / (MSR + (k-1) * MSE + k * (MSC - MSE) / n)
    else:
        print("!!! Only ICC_2_1 implemented so far")
    
    return icc

#-----------------------------------------------------------------------------
def plot_iccs_cis(icc_vals, icc_cis, feature_names, feature_class_idx_dict,
                 plot_type, icc_type_label, x_axis_shift=0,new_plot='yes',
                 plot_symbol='x',errorbar_linestyle='-',legend_fontsize=None):
    """Function for plotting radiomic ICCs and CIs, coloured by feature class.
    
    Inputs: 
        icc_vals - ICC point estimates
        icc_cis  - ICC confidence intervals
        feature_names - list of radiomic feature names
        feature_class_idx_dict - dictionary linking feature names to their class
        plot_type - 'icc_vals_only' or 'icc_vals_and_cis'
        icc_type_label - string specifying type of ICC being plotted
                       - e.g. 'ICC(2,1)','ICC(1,1)' 
        x_axis_shift - optional scalar to shift plot along x-axis
        new_plot - optional 'yes' or 'no' to generate a new plot
        plot_symbol - optional string to use as point estimate plot symbol
        errorbar_linestyle - optional string to use as linestyle for errorbars
        legend_fontsize - optional scalar for legend fontsize
    Output:
        no output returned
    """

    # Colour list for radiomic feature classes
    col_list = plt.cm.Set1(np.linspace(0,1,len(feature_class_idx_dict.keys())))
    
    # Loop over features and plot ICCs and CIs
    if new_plot == 'yes':
        plt.figure()
    else:
        pass
    
    for idx, k in enumerate(feature_class_idx_dict):
        inds = feature_class_idx_dict[k]
        vals = [icc_vals[i] for i in inds]
        cis = [icc_cis[i] for i in inds]
        if plot_type == 'icc_vals_only':
            # Not expected to use this, but kept as a reference
            plt.plot(np.array(inds)+x_axis_shift, vals, 'x', markersize = 10, 
                     c = col_list[idx,:], label = k)
        elif plot_type == 'icc_vals_and_cis':            
            # Get CIs in form suitable for plotting as errorbars
            assert np.shape(cis) == (len(inds), 2), "CIs have incorrect shape"
            err_low = [vals[i] - cis[i][0] for i in range(len(vals))]
            err_upp = [cis[i][1] - vals[i] for i in range(len(vals))]
            yerr = [err_low, err_upp]
            eb=plt.errorbar(np.array(inds)+x_axis_shift, vals, yerr = yerr, 
                         fmt = plot_symbol, c = col_list[idx,:], label = k)
            eb[-1][0].set_linestyle(errorbar_linestyle)
        else:
            print("Invalid option for plot_type")
        
        # Plot mean and SD over class
        eb=plt.errorbar(np.median(np.array(inds)+x_axis_shift), np.mean(vals), 
                     yerr = np.std(vals), fmt = 'ko')
        eb[-1][0].set_linestyle(errorbar_linestyle)
    
    plt.grid(axis = 'y', linewidth = 0.5)
    plt.ylabel(icc_type_label, fontsize = 18)
    plt.yticks(fontsize = 18)
    plt.xticks(range(len(feature_names)), feature_names, rotation = 90,
               fontsize = 10)    
    #plt.ylim(0, 1)
    plt.ylim([0, np.max(icc_cis)]) # for RC
    plt.xlim(-1, len(feature_names))    
    plt.legend(fontsize = legend_fontsize)
    plt.gcf().subplots_adjust(bottom=0.455, top=0.98)
    figfuns.figresize(25,8.2)

#-----------------------------------------------------------------------------            
def icc(repeat_measurements, icc_type, conf_level = 95):
    """Calculates intraclass correlation for repeated measurements.
    
    This replaces the depreciated function above, as it includes confidence
    intervals as well as ICC. Code is based on the icc function in the R
    package irr.
    
    References:
        McGraw & Wong, Psychol Methods 1996;1:30-46
        Weir, J Strength Cond Res 2005;19:231-240
        Gamer et al., R irr package, v.0.84.1 (https://rdrr.io/cran/irr/)
        Liljequist et al., PLoS ONE 2019, 14:7e0219854
    
    Calculation here is ICC(2,1), corresponding to:
        icc(rep_meas,model="twoway",type="agreement",unit="single") in R
    Updated to include ICC(1) and ICC(2,1) consistency 
    Notation here matches Weir 2005:
        n - number of subjects
        k - number of trials
        SStotal - total sum of squares
        MSs - subjects mean square
        MSw - within-subjects mean square
        MSt - trials mean square
        MSe - error mean square
    
    Inputs:
        repeat_measurement - n x 2 array of pair-wise measurements
        icc_type - type of ICC to calculate
        conf_level - optional argument specifying confidence level (%) for CIs
                   - defaults to 95% CIs
    Outputs:
        icc - intraclass correlation coefficient for model specified by icc_type
        cis - lower and upper confidence intervals for ICC
        p_value - p-value for test that ICC is greater than 0
        F_value - F statistic associated with pval test
    """
    # Analysis of variance calculations
    [n, k] = np.shape(repeat_measurements)
    SStotal = np.var(repeat_measurements.flatten(), ddof = 1)*(n*k - 1)
    MSs = np.var(np.mean(repeat_measurements,1), ddof = 1)*k
    MSw = np.sum(np.var(repeat_measurements, axis = 1, ddof = 1)/n)
    MSt = np.var(np.mean(repeat_measurements,0), ddof = 1)*n
    MSe = (SStotal - MSs*(n - 1) - MSt*(k - 1))/((n - 1)*(k - 1))
    
    # ICC calculations
    if icc_type == 'icc_1_1':
        # ICC(1,1) (Weir, Table 5; equivalent to McGraw & Wong, Table 4)
        # Also called ICC(1) in Liljequist et al.
        icc = (MSs - MSw)/(MSs + (k-1)*MSw)
        
        # Test statistics (McGraw & Wong, Table 8)
        r0 = 0 # Null hypothesis that icc is 0
        F_value = (MSs/MSw)*( (1 - r0)/(1 + (k - 1)*r0) )
        df1 = n - 1
        df2 = n*(k - 1)
        p_value = 1 - stats.f.cdf(F_value, df1, df2)
        
        # Confidence intervals (McGraw & Wong, Table 7)
        Fobs = MSs/MSw
        alpha = 1 - conf_level/100
        Ftab_l = stats.f.ppf(1 - alpha/2, n - 1, n*(k - 1))
        Ftab_u = stats.f.ppf(1 - alpha/2, n*(k - 1), n - 1)
        FL = Fobs/Ftab_l
        FU = Fobs*Ftab_u
        lbound = (FL - 1)/(FL + (k - 1))
        ubound = (FU - 1)/(FU + (k - 1))
        cis = [lbound, ubound]
    elif icc_type == 'icc_2_1':
        # ICC(2,1) (Weir, Table 5; equivalent to McGraw & Wong, Table 4)
        # Also called ICC(A,1) in Liljequist et al.
        icc = (MSs - MSe)/(MSs + (k - 1)*MSe + k*(MSt - MSe)/n)
        
        # Test statistics (McGraw & Wong, Table 8)
        r0 = 0 # Null hypothesis that icc is 0
        a = (k*r0)/(n*(1 - r0))
        b = 1 + (k*r0*(n - 1))/(n*(1 - r0))
        F_value = MSs/(a*MSt + b*MSe)
        
        # Possible bug in R irr? - p-val calc should use a and b with r0 = 0?
        #a = (k*icc)/(n*(1 - icc))
        #b = 1 + (k*icc*(n - 1))/(n*(1 - icc))
        v = (a*MSt + b*MSe)**2/((a*MSt)**2/(k - 1) + (b*MSe)**2/((n - 1)*(k - 1)))
        df1 = n-1
        df2 = v
        p_value = 1 - stats.f.cdf(F_value, df1, df2)
        
        # Confidence intervals (McGraw & Wong, Table 7)
        alpha = 1 - conf_level/100
        a = (k*icc)/(n*(1 - icc))
        b = 1 + (k*icc*(n - 1))/(n*(1 - icc))
        v = (a*MSt + b*MSe)**2/((a*MSt)**2/(k - 1) + (b*MSe)**2/((n - 1)*(k - 1)))
        FL = stats.f.ppf(1 - alpha/2, n - 1, v)
        FU = stats.f.ppf(1 - alpha/2, v, n - 1)
        lbound = (n*(MSs - FL*MSe))/(FL*(k*MSt + (k*n - k - n)*MSe) + n*MSs)
        ubound = (n*(FU*MSs - MSe))/(k*MSt + (k*n - k - n)*MSe + n*FU*MSs)
        cis = [lbound, ubound]
    elif icc_type == 'icc_2_1_c':
        # Consistency rather than absolute agreement
        # Called ICC(C,1) in Liljequist et al.
        icc = (MSs - MSe)/(MSs + (k - 1)*MSe)
        
        # Test statistics (McGraw & Wong, Table 8)
        r0 = 0 # Null hypothesis that icc is 0
        F_value = (MSs/MSe)*( (1 - r0)/(1 + (k - 1)*r0) )
        df1 = n - 1
        df2 = (n -1 )*(k - 1)
        p_value = 1 - stats.f.cdf(F_value, df1, df2)
        
        # Confidence intervals (McGraw & Wong, Table 7)
        Fobs = MSs/MSe
        alpha = 1 - conf_level/100
        Ftab_l = stats.f.ppf(1 - alpha/2, n - 1, (n - 1)*(k - 1))
        Ftab_u = stats.f.ppf(1 - alpha/2, (n - 1)*(k - 1), n - 1)
        FL = Fobs/Ftab_l
        FU = Fobs*Ftab_u
        lbound = (FL - 1)/(FL + (k - 1))
        ubound = (FU - 1)/(FU + (k - 1))
        cis = [lbound, ubound]
    elif icc_type == 'rc':
        # Repeatablity coefficient
        # NOT AN ICC, BUT INCLUDED HERE AS SAME CALCULATIONS ARE USED
        # See Barnhart & Barboriak, Trans Onc, 2009;2:231–235
        alpha = 1 - conf_level/100
        s_w = np.sqrt(MSw)
        rc = stats.norm.ppf(1-alpha/2)*np.sqrt(2)*s_w
        icc = rc
        
        # CIs
        num = n*(k-1)*(s_w**2)

        denom_l = stats.chi2.ppf(1-alpha/2, n*(k-1))
        lbound = stats.norm.ppf(1-alpha/2)*np.sqrt(2) * np.sqrt(num/denom_l)
        
        denom_u = stats.chi2.ppf(alpha/2, n*(k-1))
        ubound = stats.norm.ppf(1-alpha/2)*np.sqrt(2) * np.sqrt(num/denom_u)

        cis = [lbound, ubound]
        
        # Other outputs not used here
        p_value = None
        F_value = None
    elif icc_type == 'wcv':
        # Within-subject coefficient of variation
        # NOT AN ICC, BUT INCLUDED HERE AS SAME CALCULATIONS ARE USED
        # See Barnhart & Barboriak, Trans Onc, 2009;2:231–235
        alpha = 1 - conf_level/100
        s_w = np.sqrt(MSw)
        wcv = s_w/np.mean(repeat_measurements.flatten())
        icc = wcv
        
        # Other outputs not used here
        cis = [icc, icc]
        p_value = None
        F_value = None
    else:
        print("!!! Only ICC_1_1, ICC_2_1, ICC_2_1_c, RC, and wCV implemented so far")
    
    return icc, cis, p_value, F_value

#-----------------------------------------------------------------------------            
def calc_all_icc(feature_array_v1, feature_array_v2, icc_type, 
                 feature_class_idx_dict, feature_names, save_path = None, 
                 icc_conf_level = 95):
    """Function for calculating icc for all radiomics features.
    
    Inputs:
      feature_array_v1 - array of all radiomics features for visit 1
      feature_array_v2 - array of all radiomics features for visit 2
      icc_type - type of ICC to calculate
      feature_class_idx_dict - dictionary of indices for each feature class
      feature_names - list of feature names
      save_path - optional argument of path to save figures if needed
      icc_conf_level - optional argument for confidence level of ICC CIs
    Output:
      icc_vals - icc values for all features
    """
    # Check inputs
    assert len(feature_array_v1) == len(feature_array_v2), \
        "!!! feature arrays for visits 1 and 2 are not the same length"
    assert len(feature_array_v1[0]) == len(feature_array_v2[0]) == \
        len(feature_names), \
        "!!! number of features in arrays do not match number of names"
        
    # Loop over features and calculate icc and confidence intervals
    icc_vals = []
    icc_cis = []
    for i in range(len(feature_array_v1[0])):
        v1 = []
        v2 = []
        for feat_lst_1 in feature_array_v1:
            v1.append(feat_lst_1[i])
        for feat_lst_2 in feature_array_v2:
            v2.append(feat_lst_2[i])
    
        rep_meas = np.array([v1, v2])
        rep_meas = rep_meas.transpose()
        icc_result = icc(rep_meas, icc_type, icc_conf_level)
        icc_vals.append(icc_result[0])
        icc_cis.append(icc_result[1])
        
    ################
    # 14/04/20 Ignore below and use CIs calculated in new icc function
    #   Leave-one-out icc calculation to estimate confidence intervals
    #   Loop over features and calculate icc for each, using N-1 datasets
    # icc_loo_mean=[]
    # icc_loo_sd=[]
    # for i in range(len(feature_array_v1[0])):
    #     collect_icc_loo=[]
    #     for exc_ind in range(len(feature_array_v1)):
    #         v1 = []
    #         v2 = []
    #         for counter1, feat_lst_1 in enumerate(feature_array_v1):
    #             if counter1 == exc_ind:
    #                 pass
    #             else:
    #                 v1.append(feat_lst_1[i])
    #         for counter2, feat_lst_2 in enumerate(feature_array_v2):
    #             if counter2 == exc_ind:
    #                 pass
    #             else:
    #                 v2.append(feat_lst_2[i])
    #
    #         rep_meas = np.array([v1, v2])
    #         print(v1[0])
    #         print(v2[0])
    #         rep_meas = rep_meas.transpose()
    #         icc_val = icc(rep_meas, icc_type)
    #         collect_icc_loo.append(icc_val)
    #     icc_loo_mean.append(np.mean(collect_icc_loo))
    #     icc_loo_sd.append(np.std(collect_icc_loo))
    ################    
        
    # Label based on icc_type
    if icc_type == 'icc_2_1':
        icc_label = 'ICC(2,1)'
    elif icc_type == 'icc_1_1':
        icc_label = 'ICC(1,1)'
    elif icc_type == 'icc_2_1_c':
        icc_label = 'ICC(2,1)c'
    elif icc_type == 'rc':
        icc_label = 'RC'
    elif icc_type == 'wcv':
        icc_label = 'wCV'
    else:
        print("Invalid option for icc_type")
        
    # Plot icc values and CIs, coloured by feature class
    plot_type = 'icc_vals_and_cis'
    plot_iccs_cis(icc_vals, icc_cis, feature_names, feature_class_idx_dict,
                 plot_type, icc_label, legend_fontsize=14)
    if save_path == None:
        pass
    else:
        fig_file = 'icc_feat_class_' + icc_type + '.pdf'
        plt.savefig(os.path.join(save_path, fig_file))
    
    return icc_vals, icc_cis

#-----------------------------------------------------------------------------
def icc_comparison(repeat_measurement_A, repeat_measurement_B):
    """Function for Fisher's Z-test comparison of two dependent ICCs, from 
       two different measurement types, A and B.
    
    Based on Donner and Zou, The Statistician (2002) 51, Part 3, pp. 367-379.
    
    Inputs:
        repeat_measurement_A - n x r array of repeat measurements for A
        repeat_measurement_B - n x r array of repeat measurements for B
        
    Outputs:
        icc_A - ICC_1_1 for measurements A
        icc_B - ICC_1_1 for measurements B
        rhoAB - Pearson product-moment correlation between N*k_A*k_B pairs
        TZ - T-score from Fisher's Z-test
        pval - two-sided p-value for TZ
        np.sqrt(V) - standard deviation of difference

    """
    # Check inputs and get N and ks
    assert repeat_measurement_A.shape[0] == repeat_measurement_B.shape[0], \
        'Arrays for measurements A and B are not the same length'
    assert repeat_measurement_A.shape[1] > 1, \
        'Array for A must have more than one column'
    assert repeat_measurement_B.shape[1] > 1, \
        'Array for B must have more than one column'
    N =  repeat_measurement_A.shape[0]
    k_A = repeat_measurement_A.shape[1]
    k_B = repeat_measurement_B.shape[1]
    
    # ICC_1_1 for each measurement
    icc_A = icc(repeat_measurement_A, 'icc_1_1')
    icc_A = icc_A[0]
    icc_B = icc(repeat_measurement_B, 'icc_1_1')
    icc_B = icc_B[0]
    
    # rhoAB - Pearson product-moment correlation between N*k_A*k_B pairs
    collect_pairs = []
    for i in range(N):
        m_a = repeat_measurement_A[i,:];
        m_b = repeat_measurement_B[i,:];
        for j in range(k_A):    
            for k in range(k_B):
                collect_pairs.append([m_a[j], m_b[k]])
                    
    collect_pairs = np.array(collect_pairs)
    assert collect_pairs.shape[0] == N*k_A*k_B
    rhoAB = np.corrcoef(collect_pairs[:,0], collect_pairs[:,1])
    assert np.isclose(rhoAB[0,1],rhoAB[1,0]), 'corrcoef off diagonals not equal'
    rhoAB = rhoAB[0,1]
    
    # Calcuations from Section 3.1 in Donner and Zou
    Z_A = 0.5*np.log( ( 1 + ( k_A - 1 )*icc_A ) / ( 1 - icc_A ) )
    Z_B = 0.5*np.log( ( 1 + ( k_B - 1 )*icc_B ) / ( 1 - icc_B ) )
    
    pooled_A_B = np.vstack((repeat_measurement_A, repeat_measurement_B))
    rho = icc(pooled_A_B, 'icc_1_1')
    rho = rho[0]
    
    theta = 0.5*np.log( ( 1 + (k_A - 1)*rho ) / ( 1 + (k_B - 1)*rho ) )
    
    if k_A == 2:
        V_A = 1/(N - 3/2)
    else:
        V_A = k_A / (2*(k_A - 1)*(N - 2))
        
    if k_B == 2:
        V_B = 1/(N - 3/2)
    else:
        V_B = k_B / (2*(k_B - 1)*(N - 2))
        
    v_expr_num = k_A * k_B * rhoAB * rhoAB
    rho1 = rho
    rho2 = rho
    v_expr_denom = 2 * N * ( 1 + (k_A - 1)*rho1 ) * ( 1 + (k_B - 1)*rho2 )
    v_expr = v_expr_num / v_expr_denom
    V = V_A + V_B - 2*v_expr
    
    TZ = ( (Z_A - Z_B) - theta ) / np.sqrt(V)
    
    # P-value
    pval = stats.norm.sf(abs(TZ))*2
    
    return icc_A, icc_B, rhoAB, TZ, pval, Z_A, Z_B, np.sqrt(V), theta

#-----------------------------------------------------------------------------            
def bland_altman(feature_array_v1, feature_array_v2, feature_names,
                 feature_of_interest, save_path = None):
    """Function for generating Bland-Altman plots for given radiomics feature.
    
    Inputs:
      feature_array_v1 - array of all radiomics features for visit 1
      feature_array_v2 - array of all radiomics features for visit 2
      feature_names - list of feature names
      feature_of_interest - string specifying one feature, whose correlation
                            with all other features is plotted
                            e.g. feature_of_interest = 'shape_MeshVolume'
      save_path - optional argument of path to save figures if needed                          
    Output:
        rep_meas - array of repeated measures (v1 and v2) for given feature
    """
    # Check inputs
    assert len(feature_array_v1) == len(feature_array_v2), \
        "!!! feature arrays for visits 1 and 2 are not the same length"
    assert (len(feature_array_v1[0]) == len(feature_array_v2[0]) == 
            len(feature_names)), \
        "!!! number of features in arrays do not match number of number of names"
    
    # Get index for feature of interest
    f_ind = feature_names.index(feature_of_interest)
    
    # Get repeated values for given feature
    v1 = []
    v2 = []
    for feat_lst_1 in feature_array_v1:
        v1.append(feat_lst_1[f_ind])
    for feat_lst_2 in feature_array_v2:
        v2.append(feat_lst_2[f_ind])
    
    rep_meas = np.array([v1, v2])
    rep_meas = rep_meas.transpose()
        
    # Plot difference against mean
    diff = rep_meas[:,1] - rep_meas[:,0]
    diff_mean = np.mean(diff)
    val_mean = np.mean(rep_meas, 1)
    
    
    plt.figure()
    plt.plot(val_mean, diff, 'kx', markersize = 10)
    plt.plot([val_mean.min(), val_mean.max()],[diff_mean, diff_mean],
             'r--',linewidth = 0.5)
    plt.plot()
    plt.grid(axis = 'y', linewidth = 0.5)
    plt.ylabel("Visit 2 - Visit 1")
    plt.xlabel("Mean")
    plt.title(feature_of_interest)
    figfuns.figresize(7,7/1.17)
    if save_path == None:
        pass
    else:
        fig_file = 'bland_altman_diff_v_mean_' + feature_of_interest
        plt.savefig(os.path.join(save_path, fig_file))
    
    # Plot absolute difference against mean
    abs_diff = np.abs(diff)
    plt.figure()
    plt.plot(val_mean, abs_diff, 'kx', markersize = 10)
    plt.grid(axis = 'y', linewidth = 0.5)
    plt.ylabel("|Visit 2 - Visit 1|")
    plt.xlabel("Mean")
    plt.title(feature_of_interest)
    figfuns.figresize(7,7/1.17)
    if save_path == None:
        pass
    else:
        fig_file = 'bland_altman_absdiff_v_mean_' + feature_of_interest
        plt.savefig(os.path.join(save_path, fig_file))
        
    return rep_meas

#-----------------------------------------------------------------------------            
def comp_two_vals_all_features(value1, value2, feature_class_idx_dict, 
                              feature_names, val_axis_label, val1_label, val2_label,
                              plot_direction):
    """Lollypop plot to compare two values for multiple variables.
    
    E.g. compare the same set of variables measured in two different ways.
    
    Adapated from https://python-graph-gallery.com/184-lollipop-plot-with-2-groups/
    
    Inputs:
        value1 - array of values for multiple variables
        value2 - array of values for multiple variables
        feature_class_idx_dict - dictionary of indices for each feature class
        feature_names - list of feature names
        val_axis_label - value label, i.e. description of value1 and value2
        val1_label - label for value1 legend
        val2_label - label for value2 legend
        plot_direction - 'icc_x' or 'icc_y', specifying if ivalue changes should
                         be plotted horizontally or vertically
    Output:
        no output, but plot generated
    """    
    # Check input
    assert len(value1) == len(value2) == len(feature_names), \
        "Size of inputs are not equal"
    df = pd.DataFrame({'group':feature_names, 
                       'value1':value1 , 'value2':value2 })
    
    # Convert value1 and value2 to numpy arrays (needed for element-wise
    # comparison which will fail if they are lists)
    value1 = np.array(value1)
    value2 = np.array(value2)    
    
    # Skip reordering
    # Reorder it following the values of the first value:
    # ordered_df = df.sort_values(by='value1')
    ordered_df = df
    
    # Generate y/x-axis points for each feature
    # Reverse range so that first feature is plotted at the top for icc_x
    #my_range = range(1, len(df.index)+1)
    if plot_direction == 'icc_x':
        my_range = range(len(df.index),0,-1)
    if plot_direction == 'icc_y':
        my_range = range(len(df.index))
    
    # Plot both values for all features
    col_list = plt.cm.Set1(np.linspace(0,1,len(feature_class_idx_dict.keys())))
    plt.figure()
    for idx, k in enumerate(feature_class_idx_dict):
        inds = feature_class_idx_dict[k]
        my_range_inds = [my_range[i] for i in inds]
        v1 = [ordered_df['value1'][i] for i in inds]
        v2 = [ordered_df['value2'][i] for i in inds]
        if plot_direction == 'icc_x':
            plt.hlines(y = my_range_inds, xmin = v1, xmax = v2, color = 'grey')
            plt.scatter(v1, my_range_inds, edgecolors = col_list[idx,:], 
                        facecolors = 'none', label = k + '_' + val1_label)
            plt.scatter(v2, my_range_inds, edgecolors = col_list[idx,:],
                        facecolors = col_list[idx,:], label = k + '_' + val2_label)
        if plot_direction == 'icc_y':
            plt.vlines(x = my_range_inds, ymin = v1, ymax = v2, color = 'grey')
            plt.scatter(my_range_inds, v1, edgecolors = col_list[idx,:], 
                        facecolors = 'none', label = k + '_' + val1_label)
            plt.scatter(my_range_inds, v2, edgecolors = col_list[idx,:],
                        facecolors = col_list[idx,:], label = k + '_' + val2_label)
        
    # Show features where value2 is less than value1
    # Plot black star at 0 
    # x_plt = 0
    # sym = 'k*'
    # v2_ge_v1 = (value2 >= value1)
    # print(value1)
    # print(value2)
    # print(value2 >= value1)
    # v2_lower = np.logical_not(v2_ge_v1)
    # v2_lower_features = np.array(feature_names)[v2_lower]
    # print(v2_lower_features)
    # print(len(v2_lower_features))
    # v2_lower_inds = np.array(my_range)[v2_lower]
    # if plot_direction == 'icc_x':
    #     plt.plot(np.zeros_like(v2_lower_inds) + x_plt, v2_lower_inds, sym)
    # if plot_direction == 'icc_y':
    #     plt.plot( v2_lower_inds, np.zeros_like(v2_lower_inds) + x_plt, sym)
    
    # Add title and axis names
    if plot_direction == 'icc_x':
        plt.yticks(my_range, ordered_df['group'], fontsize = 6)
        plt.ylim(0, len(feature_names)+1) 
        plt.xlabel(val_axis_label)
        plt.grid(axis = 'x')
        plt.xlim((0,1))
        figfuns.figresize()
        plt.subplots_adjust(left=0.159, right=0.991, top=0.991, bottom=0.054)
    if plot_direction == 'icc_y':
        plt.xticks(my_range, ordered_df['group'], rotation = 90, fontsize = 8)
        plt.xlim(-1, len(feature_names)) 
        plt.ylabel(val_axis_label)
        plt.grid(axis = 'y')
        plt.ylim((0,1))
        plt.gcf().subplots_adjust(bottom=0.35)
        figfuns.figresize(25,8.2)
    
    plt.legend(fontsize = 6, loc = 'lower left')
    
def diff_two_vals_all_features(value1, value2, feature_class_idx_dict, 
                              feature_names, val_axis_label, val1_label, 
                              val2_label, significance_list, error_bars):
    """Bar chart plotting differnce between two values for multiple variables.
    
    E.g. compare the same set of variables measured in two different ways.
    
    Inputs:
        value1 - list of values for multiple variables
        value2 - list of values for multiple variables
        feature_class_idx_dict - dictionary of indices for each feature class
        feature_names - list of feature names
        val_axis_label - value label, i.e. description of value1 and value2
        val1_label - label for value1 legend
        val2_label - label for value2 legend
        significance_list - binary list specifying if differences are significant
        error_bars - list of values for error bars
        
    Output:
        no output, but plot generated
    """    
    # Check input
    assert len(value1) == len(value2) == len(feature_names) == len(significance_list), \
        "Size of inputs are not equal"
    df = pd.DataFrame({'group':feature_names, 
                       'value1':value1 , 'value2':value2 })
    
    # Convert value1 and value2 to numpy arrays (needed for element-wise
    # comparison which will fail if they are lists)
    value1 = np.array(value1)
    value2 = np.array(value2)    
    
    # Skip reordering
    # Reorder it following the values of the first value:
    # ordered_df = df.sort_values(by='value1')
    ordered_df = df
    
    # Generate y-axis points for each feature
    my_range = range(len(df.index))
    
    # Plot both values for all features
    col_list = plt.cm.Set1(np.linspace(0,1,len(feature_class_idx_dict.keys())))
    plt.figure()
    collect_diff = []
    collect_err = []
    for idx, k in enumerate(feature_class_idx_dict):
        inds = feature_class_idx_dict[k]
        my_range_inds = [my_range[i] for i in inds]
        v1 = [ordered_df['value1'][i] for i in inds]
        v2 = [ordered_df['value2'][i] for i in inds]
        err = [error_bars[i] for i in inds]
        plot_diff = np.array(v1) - np.array(v2)
        collect_diff.append(plot_diff)
        collect_err.append(err)
        plt.bar(my_range_inds, plot_diff,
                #color = None,
                #edgecolor = col_list[idx,:],
                color = col_list[idx,:],
                ecolor = 'gray',
                yerr = np.array(err))

    # Add star if significant
    collect_diff = np.concatenate(collect_diff, axis=0)
    collect_err = np.concatenate(collect_err, axis=0)
    for feat_ind in range(105):
        if significance_list[feat_ind] == 1:
            #plt.plot(feat_ind, collect_diff[feat_ind], 'k*', markersize = 8)
            #plt.plot(feat_ind, 0, 'k*', markersize = 8)
            if collect_diff[feat_ind]<0:
                pos = collect_diff[feat_ind] - collect_err[feat_ind] - 0.05
            elif collect_diff[feat_ind]>0:
                pos = collect_diff[feat_ind] + collect_err[feat_ind] + 0.05
            plt.plot(feat_ind, pos, 'k*', markersize = 8)
    
    # Add title and axis names
    plt.xticks(my_range, ordered_df['group'], rotation = 90, fontsize = 10)
    plt.xlim(-1, len(feature_names)) 
    plt.ylabel(val_axis_label + f'\n ({val1_label} - {val2_label})', 
               fontsize = 16)
    plt.yticks(fontsize = 18)
    plt.grid(axis = 'y')
    plt.ylim((-1.6,1.45))
    plt.gcf().subplots_adjust(bottom=0.455, top=0.98) #0.455, 0.98
    figfuns.figresize(25,8.2)