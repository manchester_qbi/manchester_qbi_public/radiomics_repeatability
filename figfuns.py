#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Module containing various functions for manipulating matplotlib figures

Created on Thu Nov 21 15:22:28 2019

@author: damienmchugh
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from skimage import measure

#-----------------------------------------------------------------------------
def mont(X, colormap = plt.cm.gray, maplims = None):
    """Display montage of 3D volume.
    Inputs: 
        X - 3D array, e.g. x,y,slices
        colormap - e.g. plt.cm.gray, plt.cm.viridis
        maplims - list of min and max values for colourmap, e.g. [0, 100]
    """
    if len(np.shape(X)) == 2:
        X = np.reshape(X,(np.shape(X)[0], np.shape(X)[1],1))
        
    m, n, count = np.shape(X)    
    mm = int(np.ceil(np.sqrt(count)))
    nn = mm
    M = np.zeros((mm * m, nn * n))

    image_id = 0
    for j in range(mm):
        for k in range(nn):
            if image_id >= count: 
                break
            sliceM, sliceN = j * m, k * n
            M[sliceM:sliceM + m,sliceN:sliceN + n] = X[:, :, image_id]
            image_id += 1
                    
    if maplims is None:
        maplims = [np.min(X), np.max(X)]
        
    plt.figure()
    plt.imshow(M, cmap=colormap, interpolation='none', clim=maplims)
    plt.axis('off')
    plt.colorbar()
    figresize()
    figmove(plt.gcf(),0,0)
    return M

#-----------------------------------------------------------------------------
def figresize(x = 13.46, y = 11.55):
    """Resize figure window."""
    fig = plt.gcf()
    fig.set_size_inches(x, y)

#-----------------------------------------------------------------------------    
def cmap(colour_map):
    """Set colourbar map."""
    plt.set_cmap(colour_map)

#-----------------------------------------------------------------------------    
def clim(min_max):
    """Set colourbar limits [min, max]."""
    plt.clim(min_max)

#-----------------------------------------------------------------------------    
def figmove(f, x, y):
    """Move figure's upper left corner to pixel (x, y)."""
    if isinstance(f,int): 
        f = plt.figure(f)
        
    backend = mpl.get_backend()
    if backend == 'TkAgg':
        f.canvas.manager.window.wm_geometry("+%d+%d" % (x, y))
    elif backend == 'WXAgg':
        f.canvas.manager.window.SetPosition((x, y))
    else:
        # This works for QT and GTK
        # You can also use window.setGeometry
        f.canvas.manager.window.move(x, y)
#-----------------------------------------------------------------------------        
def cab():
    """Close all open figures."""
    plt.close('all')

#-----------------------------------------------------------------------------        
def set_axes_radius(ax, origin, radius):
    """Set axis limits for 3D plot."""
    ax.set_xlim3d([origin[0] - radius, origin[0] + radius])
    ax.set_ylim3d([origin[1] - radius, origin[1] + radius])
    ax.set_zlim3d([origin[2] - radius, origin[2] + radius])

#-----------------------------------------------------------------------------        
def set_axes_equal(ax):
    """Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc..  This is one possible solution to Matplotlib's
    ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    """

    limits = np.array([
        ax.get_xlim3d(),
        ax.get_ylim3d(),
        ax.get_zlim3d(),
    ])

    origin = np.mean(limits, axis=1)
    radius = 0.5 * np.max(np.abs(limits[:, 1] - limits[:, 0]))
    set_axes_radius(ax, origin, radius)

#-----------------------------------------------------------------------------
def plot_3d(image, threshold=0):
    """3D rendering of masked image."""
    p = image.transpose(2,1,0)
    verts, faces, normals, values = measure.marching_cubes_lewiner(p, threshold)
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, projection='3d')
    mesh = Poly3DCollection(verts[faces], alpha=0.1)
    face_color = [0.5, 0.5, 1]
    mesh.set_facecolor(face_color)
    ax.add_collection3d(mesh)
    ax.set_xlim(0, p.shape[0])
    ax.set_ylim(0, p.shape[1])
    ax.set_zlim(0, p.shape[2])

    plt.show()

#-----------------------------------------------------------------------------            
def scroll(X):
    """Create figure for scrolling through volume."""

    class IndexTracker(object):
        def __init__(self, ax, X):
            self.ax = ax
            ax.set_title('use scroll wheel to navigate images')
    
            self.X = X
            rows, cols, self.slices = X.shape
            self.ind = self.slices//2
    
            self.im = ax.imshow(self.X[:, :, self.ind])
            self.update()
    
        def onscroll(self, event):
            print("%s %s" % (event.button, event.step))
            if event.button == 'up':
                self.ind = (self.ind + 1) % self.slices
            else:
                self.ind = (self.ind - 1) % self.slices
            self.update()
    
        def update(self):
            self.im.set_data(self.X[:, :, self.ind])
            ax.set_ylabel('slice %s' % self.ind)
            self.im.axes.figure.canvas.draw()
    
    
    fig, ax = plt.subplots(1, 1)
    
    tracker = IndexTracker(ax, X)

    fig.canvas.mpl_connect('scroll_event', tracker.onscroll)
    plt.show()
    
    return tracker    