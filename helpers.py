#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Module containing various functions used in radiomics analysis.

Created on Thu Nov 28 12:28:40 2019

@author: damienmchugh
"""
from collections import defaultdict, OrderedDict
import csv
import math
import os
import re
import sys
import time

import matplotlib.pyplot as plt
import nibabel as nib
import numpy as np
import pandas as pd
from pathlib import Path, PurePath
from PIL import Image
import pydicom
import SimpleITK as sitk
import xlrd

#-----------------------------------------------------------------------------
def write_dict_to_csv(my_dict,save_path,filename):
    """Writes dictionary to csv."""
    fn = filename + '.csv'
    fn_path = os.path.join(save_path, fn)
    df = pd.DataFrame.from_records([my_dict])
    df.to_csv(fn_path,index=False)
    return fn_path

#-----------------------------------------------------------------------------
def write_dict_to_csv_rows(my_dict,save_path,filename):
    """Alternative way of writing dictionary to csv - row based."""
    fn = filename + '.csv'
    fn_path = os.path.join(save_path, fn)
    with open(fn_path, 'w') as f:
        for key in my_dict.keys():
            f.write("%s,%s\n"%(key,my_dict[key]))

#-----------------------------------------------------------------------------
def write_dict_to_csv_hdr(my_dict,save_path,filename):
    """Alternative way of writing dictionary to csv - col based."""
    fn = filename + '.csv'
    fn_path = os.path.join(save_path, fn)
    #headers = mydict.keys()
    with open(fn_path, 'a') as f:
        writer = csv.writer(f)
        writer.writerows(zip(*([k] + my_dict[k] for k in my_dict)))

#-----------------------------------------------------------------------------
def getsubstring(fullstring, substring_start, sep = None):
    """Extract part of fullstring which starts with substring_start.

    e.g. fullstring = 'this_contains_initials - DJM00001'
         getsubstring(fullstring,'DJM') -> DJM00001
    Inputs:
        fullstring - string to query
        substring_start - string specifying the start of a substring in fullstring
        sep - optional argument specifying delimeter on which to split string
    Output:
        substr - substring
    """
    splitstr = fullstring.split(sep)
    substr = [item for item in splitstr if item.startswith(substring_start)]
    assert len(substr) == 1, "Substring is not unique"
    return substr[0]

#-----------------------------------------------------------------------------
def list_duplicates(seq):
    """Returns indices of items in list which are duplicated."""
    tally = defaultdict(list)
    for i,item in enumerate(seq):
        tally[item].append(i)
    x = (locs for key,locs in tally.items()
                            if len(locs)>1)
    return list(x)

#-----------------------------------------------------------------------------
def roi_file_finder(path_to_roi, roi_file_regex):
    """Function to determine ROI(s) to use, based on contents of directory.

    Inputs:
        path_to_roi - path of directory containing ROIs
        roi_file_regex - regex pattern of ROI filenames we want
    Output:
        roi_list - list of ROI filenames matching regex pattern
    """
    # First, get a list of paths to all ROI file matching regex
    roi_files = \
        [f for f in os.listdir(path_to_roi) if re.match(roi_file_regex, f)]
    roi_list_tmp = [os.path.join(path_to_roi, r) for r in roi_files]

    # Note if no files are found and end here
    if not roi_list_tmp:
        print('No ' + roi_file_regex + ' ROIs found in ' + path_to_roi)
        return roi_list_tmp

    # Remove ROIs if not created in 2020
    year_wldcrd = '2020'
    roi_list = []
    for r in roi_list_tmp:
        fle_date_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(os.path.getmtime(r)))
        fle_date = fle_date_time.split()[0]
        if year_wldcrd in fle_date:
            roi_list.append(r)

    return roi_list

    #####
    ## Ignore code below - early version where ROIs were chosen in a way
    ## which is incompatible with the current naming and date convention
    #####
    # If only one file, we're done; otherwise work out which one(s) to use
    # if len(roi_list) == 1:
    #     roi_files = copy.deepcopy(roi_list)
    # else:
    #     roi_num = []
    #     for roi_cnt in roi_list:
    #         roi_name = os.path.split(roi_cnt)[-1]
    #         strs = re.findall(r'\w\d+', roi_name)
    #         for substrs in strs:
    #             if '_' in substrs:
    #                 roi_num.append(re.findall(r'\d+', substrs)[0])

    #     if not roi_num:
    #         # Single ROI but edited, so file name does not contain a number
    #         roi_files = copy.deepcopy(roi_list)
    #         file_times = (os.path.getmtime(f) for f in roi_files)
    #         file_times = list(file_times)
    #         oldest_file = np.argmin(file_times)
    #         file_to_exclude = roi_files[oldest_file]
    #         roi_files.remove(file_to_exclude)

    #     elif len(roi_num) > 0 and len(np.unique(roi_num)) == len(roi_list):
    #         # Straightforward case of multiple unedited ROIs
    #         roi_files = roi_list

    #     elif len(roi_num) > 0 and len(np.unique(roi_num)) != len(roi_list):
    #         # Indicates some ROIs have been edited, so remove oldest
    #         # Start with all and then remove
    #         roi_files = copy.deepcopy(roi_list)
    #         # First, get indices of duplicated ROIs
    #         duplicate_ind = list_duplicates(roi_num)
    #         # Loop over duplicates and exclude oldest
    #         for i in duplicate_ind:
    #             duplicated_rois = [roi_list[ind] for ind in i]
    #             file_times = (os.path.getmtime(dup) for dup in duplicated_rois)
    #             file_times = list(file_times)
    #             oldest_file = np.argmin(file_times)
    #             file_to_exclude = duplicated_rois[oldest_file]
    #             roi_files.remove(file_to_exclude)

#-----------------------------------------------------------------------------
def excel_col_row(excel_sheet_path, col_or_row, hdr):
    """Read a column or row from excel sheet, given a column or row header.

    Inputs:
        excel_sheet_path - path to excel file
        col_row - specifies if we want to read a 'col' or 'row'
        col_row_hdr - header for column or row
    Output:
        excel_col_row_vals - list with cell values for given column or row
    """
    # Define subfunction for looping over sheet
    def col_row_subfn(xls_sheet, col_or_row, hdr):
        hdr_match = 0
        for i in range(xls_sheet.ncols):
                for j in range(xls_sheet.nrows):
                    if hdr in str(xls_sheet.cell_value(j,i)):
                        hdr_match += 1
                        if col_or_row == 'col':
                            excel_col_row_vals = xls_sheet.col_values(i)
                        elif col_or_row == 'row':
                            excel_col_row_vals = xls_sheet.row_values(j)

        assert hdr_match == 1, \
            "Header not found, or multiple matches found"

        return excel_col_row_vals

    # Check input
    assert col_or_row == 'col' or col_or_row == 'row', \
        "col_or_row must be 'col' or 'row'"

    # Read excel spreadsheet
    wb = xlrd.open_workbook(excel_sheet_path)
    sheet = wb.sheet_by_index(0)

    # Get row range based on id
    # - i.e. only consider rows which contain relevant info
    ids_col = col_row_subfn(sheet, 'col', 'Patient ID')
    ind = [i for i, x in enumerate(ids_col) if 'P' in x]

    # Loop over columns and rows, find header, and read corresponding col/row
    vals_all = col_row_subfn(sheet, col_or_row, hdr)
    vals = [vals_all[i] for i in ind]

    return vals

#-----------------------------------------------------------------------------
def fix_mask(image_path, mask_path, output_filename):
    """Function for writing new mask file, with dimensions and slice thickness
    in header matching that in image file.
    """

    # Load image and mask
    img = nib.load(image_path)
    msk = nib.load(mask_path)

    # Resize mask to match image
    img_inplane = img.shape[0:2]
    msk_data = np.zeros(img.shape)[:,:,:,0]
    msk_resized = msk
    #msk = msk[:,:,:,0]
    msk = np.array(msk.get_fdata())
    msk_tmp = np.zeros(msk.shape)
    for i in range(0,msk.shape[2]):
        msk_tmp = msk[:,:,i]>0
        msk_data[:,:,i] = \
            np.array(Image.fromarray(msk_tmp).resize(img_inplane,Image.BICUBIC))

    msk_data = msk_data > 0

    # Overwrite mask file dimensions and slice thickness with that in image file
    msk_resized.header['dim'][1:4] = img.shape[0:3]
    msk_resized.header['pixdim'][3] = img.header['pixdim'][3]

    # Create new analyze file and save
    msk_new = nib.Spm2AnalyzeImage(msk_data, img.affine, msk_resized.header)
    nib.save(msk_new, output_filename)

#-----------------------------------------------------------------------------
def sitk_analyze_to_array(sitk_img):
    """Converts analyze image read with sitk to an array with conventional orientation.
    N.B. Orientation transforms work with .img files from QBI
        - will need to check for files from other sources
    Input:
        sitk_img - SimpleITK.Image, i.e. read using sitk.ReadImage
    Output:
        im_array - 3D image array, which can be passed to mont
    """
    im_array = sitk.GetArrayFromImage(sitk_img)
    im_array = np.swapaxes(im_array, 0, 2)
    im_array = np.rot90(im_array)
    return im_array

#-----------------------------------------------------------------------------
def remove_keymap_conflicts(new_keys_set):
    for prop in plt.rcParams:
        if prop.startswith('keymap.'):
            keys = plt.rcParams[prop]
            remove_list = set(keys) & new_keys_set
            for key in remove_list:
                keys.remove(key)

#-----------------------------------------------------------------------------
def multi_slice_viewer(volume):
    remove_keymap_conflicts({'j', 'k'})
    fig, ax = plt.subplots()
    ax.volume = volume
    ax.index = volume.shape[0] // 2
    ax.imshow(volume[ax.index])
    fig.canvas.mpl_connect('key_press_event', process_key)

#-----------------------------------------------------------------------------
def process_key(event):
    fig = event.canvas.figure
    ax = fig.axes[0]
    if event.key == 'j':
        previous_slice(ax)
    elif event.key == 'k':
        next_slice(ax)
    fig.canvas.draw()

#-----------------------------------------------------------------------------
def previous_slice(ax):
    volume = ax.volume
    ax.index = (ax.index - 1) % volume.shape[0]  # wrap around using %
    ax.images[0].set_array(volume[ax.index])

#-----------------------------------------------------------------------------
def next_slice(ax):
    volume = ax.volume
    ax.index = (ax.index + 1) % volume.shape[0]
    ax.images[0].set_array(volume[ax.index])

#-----------------------------------------------------------------------------
def histogram_matching(input_image, reference_image, hist_bins, match_points,
                       threshold_mean):
    """Wrapper for sitk histogram matching.

    See https://itk.org/SimpleITKDoxygen/html/classitk_1_1simple_1_1HistogramMatchingImageFilter.html#details
    Inputs:
        input_image - sitk image which is to be modified
        reference_image  - sitk image which input will be matched to
        hist_bins - number of bins for input and reference histograms
        match_points - dumber of quantile values to be matched
        threshold_mean - 'on' specifies background exclusion (below mean)
                       - 'off' includes all voxels
    Output:
        input_image_histmatch - sitk image of normalised input_image
    """
    matcher = sitk.HistogramMatchingImageFilter()
    matcher.SetNumberOfHistogramLevels(hist_bins)
    matcher.SetNumberOfMatchPoints(match_points)
    if threshold_mean == 'on':
        matcher.ThresholdAtMeanIntensityOn()
    elif threshold_mean == 'off':
        matcher.ThresholdAtMeanIntensityOff()
    else:
        sys.exit('threshold_mean must be \'on\' or \'off\'')

    input_image_histmatch = matcher.Execute(input_image, reference_image)
    return input_image_histmatch

#-----------------------------------------------------------------------------
def dicom_attribute_info(file_path, dcm_attributes, output_filename = None):
    """Returns values from specific DICOM attributes.

    Parameters
    ----------
    file_path : PurePath or string
        Name of DICOM file to read
    dcm_attributes : list
        List of specific DICOM attributes to get values for.
    output_filename : bool, optional
        Optional argument to include filename in dictionary output;
        default is to not include it.

    Raises
    ------
    TypeError
        Raised if file_path is not PurePath or string

    Returns
    -------
    attr_values : dict (None if file_path is not a DICOM file)
        Dictionary of attribute names and values

    """
    #%% Make sure file_path is a string
    if isinstance(file_path, PurePath):
        file_path = str(file_path.resolve())
    elif isinstance(file_path, str):
        pass
    else:
        raise TypeError('file_path is not PurePath or string')

    #%% Get values for specific attributes, ignorning any non-DICOM files
    try:
        ds = pydicom.dcmread(file_path, specific_tags=dcm_attributes)
    except (pydicom.errors.InvalidDicomError, AttributeError):
        return None

    vals = []
    for attr in dcm_attributes:
        # If tag doesn't exist for a given file, set the value to 0
        get_val = ds.get(attr, 0)
        vals.append(get_val)

    #%% Create dictionary, and include file_path if needed
    attr_values = dict(zip(dcm_attributes, vals))
    if output_filename is None:
        pass
    else:
        attr_values['filepath'] = file_path

    attr_values = OrderedDict(attr_values)

    return attr_values

#-----------------------------------------------------------------------------
def to_precision(x,p):
    """
    returns a string representation of x formatted with a precision of p

    Based on the webkit javascript implementation taken from here:
    https://code.google.com/p/webkit-mirror/source/browse/JavaScriptCore/kjs/number_object.cpp
    """

    x = float(x)

    if x == 0.:
        return "0." + "0"*(p-1)

    out = []

    if x < 0:
        out.append("-")
        x = -x

    e = int(math.log10(x))
    tens = math.pow(10, e - p + 1)
    n = math.floor(x/tens)

    if n < math.pow(10, p - 1):
        e = e -1
        tens = math.pow(10, e - p+1)
        n = math.floor(x / tens)

    if abs((n + 1.) * tens - x) <= abs(n * tens -x):
        n = n + 1

    if n >= math.pow(10,p):
        n = n / 10.
        e = e + 1

    m = "%.*g" % (p, n)

    if e < -2 or e >= p:
        out.append(m[0])
        if p > 1:
            out.append(".")
            out.extend(m[1:p])
        out.append('e')
        if e > 0:
            out.append("+")
        out.append(str(e))
    elif e == (p -1):
        out.append(m)
    elif e >= 0:
        out.append(m[:e+1])
        if e+1 < len(m):
            out.append(".")
            out.extend(m[e+1:])
    else:
        out.append("0.")
        out.extend(["0"]*-(e+1))
        out.append(m)

    return "".join(out)
