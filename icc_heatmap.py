#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Module containing Function for plotting ICCs as heat map.

@author: damienmchugh
"""
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

def icc_heatmap(parent_dir, paths_no_norm, paths_norm, sequence_labels):
    """Function for plotting ICC values from different images/normalisations 
       as a heatmap.
        
        Inputs:
            parent_dir - path to directory containing non-normalised and normalised ICC datasets
            paths_no_norm - list of paths to ICCs from non-normalised images
            paths_norm - list of paths to ICCs from normalised images
            sequence_labels - list of strings specifying sequence type for each path, e.g.
                              ['T1W\nprecon', 'T2W\nprecon', 'qT1-map\nprecon', 'T1W\npostcon']
        Outputs:
            no output - generates plots and prints mean CoV for each feature class
        """
        
    #%% Check input
    assert len(paths_no_norm) == len(paths_norm), \
        "!!! number of paths must be the same for non-normalised and normalised datasets"
        
    assert len(paths_no_norm) == len(sequence_labels), \
        "!!! number of paths must be the same as number of sequence labels"
    
    #%% Load ICCs for all datasets
    num_datasets = len(paths_no_norm)
    collect_icc_nonorm = np.zeros((105, num_datasets))
    collect_icc_norm = np.zeros((105, num_datasets))
    for i in range(num_datasets):
        # No norm
        nonorm_path = paths_no_norm[i]
        nonorm_str = nonorm_path.find('T')
        nonorm_str = nonorm_path[nonorm_str:]
        nonorm_icc_path = os.path.join(parent_dir, nonorm_path, 'icc_vals_transform_boxcox',
                                f'icc_1_1_{nonorm_str}_all.txt')
        icc_nonorm = np.loadtxt(nonorm_icc_path)
    
        collect_icc_nonorm[:,i] = icc_nonorm[:,0]
    
        # Norm
        norm_path = paths_norm[i]
        norm_str = norm_path.find('T')
        norm_str = norm_path[norm_str:]
        norm_icc_path = os.path.join(parent_dir, norm_path, 'icc_vals_transform_boxcox',
                                f'icc_1_1_{norm_str}_all.txt')
        icc_norm = np.loadtxt(norm_icc_path)
    
        collect_icc_norm[:,i] = icc_norm[:,0]
    
    #%% Plot heatmaps
    # Sort labels
    feature_class_idx_dict = np.load(os.path.join(parent_dir,
                                                  'feat_class_index_dictionary.npy'),
                                     allow_pickle='TRUE').item()
    feature_class_lines = []
    for key in feature_class_idx_dict:
        feature_class_lines.append(feature_class_idx_dict[key][-1] + 1)
    
    feature_class_lines.pop() # remove last line
    
    fn = np.loadtxt(os.path.join(parent_dir, 'feature_names.txt'),
                               dtype='str')
    feature_names = []
    class_names = []
    for i in fn:
        feature_names.append(i.split('_')[1]) # Remove class name
        class_names.append(i.split('_')[0])
    
    u, ind = np.unique(class_names, return_index=True)
    class_names = u[np.argsort(ind)]
    # Calculate position of class labels
    #label_pos = [0.9, 0.75, 0.60, 0.4, 0.23, 0.09, 0.0]
    l = np.linspace(0,1,105)
    ll = [0] + feature_class_lines + [105]
    lld = np.diff(ll)/2
    p = []
    for i in range(len(class_names)):
        p.append((ll[i] + lld[i])*np.diff(l)[0])
    label_pos = 1-np.array(p)
    
    # Plot nonorm and norm ICCs
    for i in range(2):
        if i == 0:
            x = collect_icc_nonorm
            title_str = 'No normalisation'
        elif i == 1:
            x = collect_icc_norm
            title_str = 'Normalisation'
    
        df = pd.DataFrame(data = x,
                          columns = sequence_labels)
        fig, ax = plt.subplots(figsize=(6,10))
        h = sns.heatmap(df, linewidth=1, cmap='viridis', vmin=0.0, vmax=1,
                    ax=ax, yticklabels=feature_names,
                    cbar_kws={'label':'ICC(1,1)'})
        ax.hlines(feature_class_lines, *ax.get_xlim(), linestyles='--')
        plt.subplots_adjust(left=0.4, right=0.9, top=0.96, bottom=0.05)
        plt.title(title_str)
        cbar = ax.collections[0].colorbar
        cbar.ax.tick_params(labelsize = 8)
        h.axes.set_yticklabels(h.axes.get_ymajorticklabels(), fontsize = 6)
        h.axes.set_xticklabels(h.axes.get_xmajorticklabels(), fontsize = 8)
        #plt.xticks(rotation=45)
        for i in range(len(class_names)):
            plt.text(-0.9, label_pos[i], class_names[i], rotation='vertical',
                     transform=ax.transAxes, verticalalignment='center')
    
    # Plot CoV across sequences/normalisations for each feature
    all_iccs = np.column_stack((collect_icc_nonorm,collect_icc_norm))
    all_covs = np.std(all_iccs,axis=1,ddof=1)/np.mean(all_iccs,axis=1)
    all_covs = 100*np.reshape(all_covs, (105,1))
    # Add 3 columns of zeros, so widths match plots above
    all_covs = np.column_stack((all_covs,np.zeros((105,3))))
    # CoV heatmap plot not used - plot as points instead (see below)
    # fig, ax = plt.subplots(figsize=(6,10))
    # hh = sns.heatmap(all_covs, linewidth=1, cmap='viridis',
    #                 ax=ax, yticklabels=feature_names, annot=True,
    #                 cbar_kws={'label':'CoV (%)'},
    #                 annot_kws={'fontsize':5})
    # ax.hlines(feature_class_lines, *ax.get_xlim(), linestyles='--')
    # plt.subplots_adjust(left=0.4, right=0.9, top=0.96, bottom=0.05)
    # plt.title('CoV across all datasets')
    # cbar = ax.collections[0].colorbar
    # cbar.ax.tick_params(labelsize = 8)
    # hh.axes.set_yticklabels(hh.axes.get_ymajorticklabels(), fontsize = 6)
    # hh.axes.set_xticklabels(hh.axes.get_xmajorticklabels(), fontsize = 8)
    # for i in range(len(class_names)):
    #         plt.text(-0.9, label_pos[i], class_names[i], rotation='vertical',
    #                  transform=ax.transAxes, verticalalignment='center')
    
    # Plot CoVs as points, rather than heatmap
    col_list = plt.cm.Set1(np.linspace(0,1,len(feature_class_idx_dict.keys())))
    fig, ax = plt.subplots(figsize=(4,10))
    ypos = np.flipud(np.linspace(0.5,104.5,105))
    covs_to_plot = all_covs[:,0]
    for idx, k in enumerate(feature_class_idx_dict):
        inds = feature_class_idx_dict[k]
        plt.plot(covs_to_plot[inds], ypos[inds], 'x', color = col_list[idx,:])
        print(f'Mean CoV for {k} class: {np.mean(covs_to_plot[inds])}')
    
    plt.xlim(0,30)
    plt.ylim(0,105)
    plt.grid(axis = 'x')
    plt.yticks(ypos)
    ax.set_xticklabels([0,10,20,30,40], fontsize = 8)
    plt.xlabel('CoV (%)', fontsize=8)
    ax.hlines(105-np.array(feature_class_lines), *ax.get_xlim(), linestyles='--')
    plt.subplots_adjust(left=0.4, right=0.9, top=0.96, bottom=0.05)