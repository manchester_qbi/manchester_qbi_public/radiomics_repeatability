Directories containing extracted radiomics features.

Each directory is for a different MR sequence, with or without image normalisation applied, and contain subdirectories for feature values with or without Box-Cox transformations applied.

Within each subdirectoy, 210 .txt files contain values for each repeat scan (v1/v2) and each feature (indexed 0-104, with feature names given in feature_names.txt).

For example, Analysis_T1_precon_Params_normalise_false/feature_values_boxcox/v1_feat_0.txt contains Box-Cox transformed values for shape_Elongation, from non-normalised T1W pre-contrast images.
