cd C:\isbe\code\manchester_qbi\public
git clone https://gitlab.com/manchester_qbi/manchester_qbi_public/radiomics_repeatability.git

conda create --name radiomics_env python=3.6
conda activate radiomics_env
conda install -n radiomics_env numpy=1.18.1
conda install -c anaconda statsmodels
conda install -c radiomics pyradiomics
conda install matplotlib
conda install scikit-image
conda install nibabel
conda install xlrd
conda install --name radiomics_env ipykernel -y
