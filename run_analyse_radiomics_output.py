#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Module for run_analyse_radiomics_output function to analyse radiomics output.

Created on Wed Apr 15 14:46:05 2020

@author: damienmchugh
"""
import collections
import glob
import os
import re
import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import stats
import statsmodels.api as sm

import analyse_radiomics_functions as radfun
import figfuns
import helpers

def run_analyse_radiomics_output(radiomics_dir, radiomics_output_file,
                                 invivo_sims, save_figs, save_icc_vals,
                                 visits_to_use, rois_to_use,
                                 correlation_feature_of_interest,
                                 icc_type_list, icc_conf_level,
                                 bland_altman_feature_of_interest,
                                 transform_type):
    """Main function for analysing features output from radiomics.

    Inputs:
        radiomics_dir - path to radiomics analysis directory
        radiomics_output_file  - file name of radiomics feature results
        invivo_sims - 'invivo' or 'sims'
        save_figs - 'yes' or 'no'
        save_icc_vals - 'yes' or 'no'
        visits_to_use - ('1','2') or ('1') or ('2')
        rois_to_use - 'all' or 'one'
        correlation_feature_of_interest - specify feature whose correlations
                                          are to be evaluated
        icc_type_list - list of icc types to calculate ['icc_1_1', 'icc_2_1']
        icc_conf_level - confidence level for ICC CI calculations (%)
        bland_altman_feature_of_interest - feature for bland-altman plots
        transform_type - 'none' or 'boxcox' to specify transform for features
                         in ICC calculation
    Output:
        no output returned, but figures and values saved depending on inputs
    """
    #%% Set variables depending on data and image type
    assert os.path.isdir(radiomics_dir) == True, \
        "!!! radiomics_dir does not exist"
    if invivo_sims == 'invivo':
        dir_wldcrd = 'A*'
        if 'T2' in radiomics_dir:
            img_roi_dict = {'T2W': 'T2W_joc[0-9](.*)_pymask'}
        elif 'T1_precon' in radiomics_dir:
            img_roi_dict = {'T1W_precon': 'T1W_joc[0-9](.*)_pymask'}
        elif 'T1_postcon' in radiomics_dir:
            img_roi_dict = {'T1W_postcon': 'T1W_joc[0-9](.*)_pymask'}
        elif 'T1_map' in radiomics_dir:
            # Same subset of ROIs for T1_postcon apply here
            img_roi_dict = {'T1_map_header_fixed':
                            'T1W_joc[0-9]_for_T1_map_pymask'}
        else:
           sys.exit('Unrecognised ims type in radiomics_dir')
    elif invivo_sims == 'sims':
        dir_wldcrd = 'signal_homog_visits_id*'
        img_roi_dict = {'img_syn': 'img_mask'}
    else:
        sys.exit('!!! invalid option for invivo_sims')

    #%% Loop over patients, visits, and rois, collecting csv outputs
    collect_ptn_cnt = []
    collect_vst_cnt = []
    collect_lsn_cnt = []
    collect_all_vals = []
    collect_all_keys = []
    feat_vals_v1 = []
    feat_vals_v2 = []
    roi_total_count = 0
    ptn_cnt = 0
    for pt in sorted(glob.glob(os.path.join(radiomics_dir, dir_wldcrd))):
        ptn_cnt += 1
        pt_path = os.path.join(radiomics_dir, pt)
        pt_id = os.path.split(pt)[-1]
        #print('********************')
        #print(pt_id)
        vst_cnt = 0
        for visit in sorted(glob.glob(os.path.join(pt_path,'*'))):
            visit_path = os.path.join(pt_path,visit)
            visit_name = os.path.split(visit_path)[-1]
            if visit_name in visits_to_use:
                vst_cnt += 1
                visit_num = re.findall(r'\d+', visit_name)[0]
                #print('----')
                #print('Visit ' + visit_num)
                for scan in list(img_roi_dict):
                    #print(scan)
                    roi_dir_path = os.path.join(pt_path,visit_path,scan)
                    roi_dirs = [f for f in os.listdir(roi_dir_path)
                              if re.match(img_roi_dict[scan], f)]
                    roi_dirs = sorted(roi_dirs)
                    if rois_to_use == 'all':
                        pass
                    elif rois_to_use == 'one':
                        # Make sure roi_dirs is a list, not a string!
                        roi_dirs = [roi_dirs[0]]
                    else:
                        sys.exit('!!! invalid option for rois_to_use')

                    lsn_cnt = 0
                    for roi in roi_dirs:
                        #print(roi)
                        # Collect patient, visit, and lesion indices
                        collect_ptn_cnt.append(ptn_cnt)
                        collect_vst_cnt.append(vst_cnt)
                        lsn_cnt += 1
                        collect_lsn_cnt.append(lsn_cnt)

                        # Read radiomics output file
                        roi_path = os.path.join(pt_path,visit_path,scan,roi)
                        rad_file = os.path.join(roi_path,radiomics_output_file)
                        csv = pd.read_csv(rad_file)
                        csv = csv.to_dict()
                        csv = collections.OrderedDict(csv)

                        # Remove 'diagnostics_*' parameters
                        feat_rm = []
                        for key in csv:
                            if 'diagnostics' in key:
                                feat_rm.append(key)
                        for f in feat_rm:
                            del csv[f]

                        # Get radiomics values and store keys
                        rad_key = []
                        rad_val = []
                        for key in csv:
                            rad_key.append(key)
                            rad_val.append(csv[key][0])

                        # Check we have the expected number of values
                        assert len(rad_val)==105, \
                            "!!! parameter list does not have 105 elements"

                        # Collect keys to check ordering is the same for all
                        # files
                        collect_all_keys.append(rad_key)

                        # Collect all values
                        collect_all_vals.append(rad_val)

                        # Collect according to visit
                        if visit_num == '1':
                            feat_vals_v1.append(rad_val)
                        elif visit_num == '2':
                            feat_vals_v2.append(rad_val)

                        # Count
                        roi_total_count += 1

    print('********************')
    print(f'Total number of rois: {roi_total_count}')

    #%% Patient, visit, lesion info - all V1, then all V2
    grp_idx = pd.DataFrame({'Patient': collect_ptn_cnt,
                            'Visit': collect_vst_cnt,
                            'Lesion': collect_lsn_cnt})
    grp_idx.sort_values(by = ['Visit', 'Patient'], inplace = True)
    # Write table
    fn = 'patient_visit_lesion_indices.txt'
    grp_idx.to_csv(os.path.join(radiomics_dir, fn),
                    sep = '\t', index = False)
    # # Plot lesion number distribution
    # v1 = grp_idx[grp_idx['Visit']==1]
    # ls = v1.groupby('Patient')['Lesion'].count()
    # bins = np.arange(0, ls.values.max() + 1.5) - 0.5
    # plt.hist(ls, bins=bins, rwidth=0.2, edgecolor = 'k', facecolor='w')
    # plt.xlim([0.5, 5.5])
    # plt.xlabel('Number of lesions')
    # plt.ylabel('Number of patients')


    #%% Check key ordering was the same in all files
    assert all(f == collect_all_keys[0] for f in collect_all_keys), \
        "!!! key ordering is not the same for all radiomics_output.csv files"

    #%% Simplfy/rename lists for values and names (remove 'original' prefix)
    feat_names_tmp = collect_all_keys[0]
    feat_names = [i.replace('original_','') for i in feat_names_tmp]
    #with open('feature_names.txt', 'w') as f:
    #            np.savetxt(f, feat_names, fmt = '%s')

    #%% Given the above, get indices for each feature class
    feat_class = ('shape', 'firstorder', 'glcm', 'glrlm', 'glszm', 'gldm',
                  'ngtdm')
    feat_class_inds = dict()
    for feat in feat_class:
        feat_ind=[]
        for idx, key in enumerate(feat_names):
            if feat in key:
                feat_ind.append(idx)
            feat_class_inds[feat] = feat_ind

    feat_vals_to_use = []
    feat_vals_to_use.extend(feat_vals_v1)
    feat_vals_to_use.extend(feat_vals_v2)
    # np.save('feat_class_index_dictionary.npy', feat_class_inds)
    # np.load('feat_class_index_dictionary.npy',allow_pickle='TRUE').item()

    #%% Set output path if figures are being saved
    if save_figs == 'yes':
        visit_str = '_'.join(visits_to_use)
        fig_dir = (f'figures_rois_{rois_to_use}_visits_{visit_str}_'
                   f'transform_{transform_type}')
        save_path = os.path.join(radiomics_dir, fig_dir)
        os.makedirs(save_path, exist_ok=True)
    elif save_figs == 'no':
        save_path = None
    else:
        sys.exit('!!! invalid option for save_figs')

    #%% Calculate correlations
    # corr_mat, pval_mat, corr_feat, pval_feat = \
    #     radfun.calc_correlations(feat_vals_to_use, feat_names, feat_class_inds,
    #                               correlation_feature_of_interest, save_path)

    #%% Set threshold for significance in Shapiro-Wilk tests
    apply_bonferroni = 'yes' # 'yes', 'no', 'yes_105*4'
    if apply_bonferroni == 'yes':
        pthresh = 0.05/len(rad_val)
    elif apply_bonferroni == 'yes_105*4':
        pthresh = 0.05/(len(rad_val)*4)
    elif apply_bonferroni == 'no':
        pthresh = 0.05
    else:
        sys.exit('!!! invalid option for apply_bonferroni')

    #%% Plot histograms for all features, and test for normality
    # N.B. v1 and v2 are treated separately here
    # N.B. these histograms are always for original (untransformed) features
    feat_vals_1 = np.array(feat_vals_v1)
    feat_vals_2 = np.array(feat_vals_v2)
    fig, axs = plt.subplots(10, 11, sharey=True)
    axs = axs.flatten()
    for feat_idx in range(feat_vals_1.shape[1]):
        v1 = feat_vals_1[:,feat_idx]
        v2 = feat_vals_2[:,feat_idx]
        lw_lim = np.min( (np.min(v1), np.min(v2)) )
        up_lim = np.max( (np.max(v1), np.max(v2)) )
        max_sd = np.max( (np.std(v1, ddof=1), np.std(v2, ddof=1)) )
        mean_median = np.mean( (np.median(v1), np.median(v2)) )
        v1_norm_w, v1_norm_p = stats.shapiro(v1)
        v2_norm_w, v2_norm_p = stats.shapiro(v2)

        axs[feat_idx].hist(v1, bins = 15, range = (lw_lim, up_lim),
                                  histtype = 'step', color = 'C0',
                                  linestyle = 'solid', label = 'V1')
        axs[feat_idx].hist(v2, bins = 15, range = (lw_lim, up_lim),
                                  histtype = 'step', color = 'C1',
                                  linestyle = 'dashed', label = 'V2')
        axs[feat_idx].set_xticklabels([])
        axs[feat_idx].set_title(feat_names[feat_idx], fontsize=4, pad=-15)

        if feat_idx == 0:
            axs[feat_idx].legend(fontsize=6)

        if v1_norm_p > pthresh:
            axs[feat_idx].text(0.05, 0.85, f'p={v1_norm_p:.4f}', color='C0',
                                fontsize=8, transform=axs[feat_idx].transAxes)

        if v2_norm_p > pthresh:
            axs[feat_idx].text(0.05, 0.70, f'p={v2_norm_p:.4f}', color='C1',
                                fontsize=8, transform=axs[feat_idx].transAxes)

        plt.ylim([0, 80])
        plt.xlim([mean_median - 5*max_sd, mean_median + 5*max_sd])
    for i in range(feat_vals_1.shape[1],10*11):
        axs[i].axis("off")
        figfuns.figresize()
        plt.subplots_adjust(left=0.027, right=0.990, top=0.990, bottom=0.030)

    # Save if needed
    if save_figs == 'yes':
        plt.savefig(os.path.join(save_path,
                                 'hists_105features_separate_v1_v2.pdf'))

    #%% qq plots, histograms, and Box-Cox transforms
    # N.B. all plots and calculations here use pooled v1 and v2 data
    fig, axs = plt.subplots(10, 11, num = 'qq_boxcox')
    axs = axs.flatten()
    fig2, axs2 = plt.subplots(10, 11, sharey=True, num = 'boxcoxnorm')
    axs2 = axs2.flatten()
    fig3, axs3 = plt.subplots(10, 11, sharey=True, num = 'orighist')
    axs3 = axs3.flatten()
    fig4, axs4 = plt.subplots(10, 11, sharey=True, num = 'boxcoxhist')
    axs4 = axs4.flatten()
    fig5, axs5 = plt.subplots(10, 11, num = 'qq_orig')
    axs5 = axs5.flatten()
    fig6, axs6 = plt.subplots(10, 11, num = 'bland-altman')
    axs6 = axs6.flatten()
    collect_tf_lmbda = []
    feat_vals_v1_tf = []
    feat_vals_v2_tf = []
    count_orig_normal = 0
    count_post_transform_total_normal = 0
    box_cox_applied = np.zeros((feat_vals_1.shape[1]))
    for feat_idx in range(feat_vals_1.shape[1]):
        v1 = feat_vals_1[:,feat_idx]
        v2 = feat_vals_2[:,feat_idx]
        v_1_2 = np.concatenate((v1, v2))
        lw_lim = np.min(v_1_2)
        up_lim = np.max(v_1_2)
        mean_val = np.mean(v_1_2)
        std_val = np.std(v_1_2, ddof=1)
        orig_cov = 100*std_val/mean_val
        max_sd = np.max(std_val)

        # Histogram and normality test for original distribution
        v12_orig_w, v12_orig_p = stats.shapiro(v_1_2)
        axs3[feat_idx].hist(v_1_2, bins = 15, range = (lw_lim, up_lim),
                                  histtype = 'step', color = 'k',
                                  linestyle = 'solid', label = 'V_1_2')
        #axs3[feat_idx].set_xticklabels([])
        axs3[feat_idx].set_title(feat_names[feat_idx], fontsize=4, pad=-15)
        #if feat_idx == 0:
        #    axs3[feat_idx].legend(fontsize=6)

        if v12_orig_p > pthresh:
            axs3[feat_idx].text(0.05, 0.70, f'p = {v12_orig_p:.4f}', color='k',
                                fontsize=8, transform=axs3[feat_idx].transAxes)

        # Add CoV (v1 and v2)
        #axs3[feat_idx].text(0.05, 0.85, f'CoV = {orig_cov:.0f} %',
        #                    color='k',fontsize=8,
        #                    transform=axs3[feat_idx].transAxes)
        axs3[feat_idx].tick_params(axis="x", pad=-0.5, labelsize=4)
        axs3[feat_idx].tick_params(axis="y", labelsize=6)
        axs3[feat_idx].set_ylim([0,200])
        axs3[feat_idx].set_yticks([0, 75, 150])
        axs3[feat_idx].set_xlim([mean_val - 5*max_sd, mean_val + 5*max_sd])
        # If non-normal, find optimal Box-Cox transform
        if v12_orig_p < pthresh:
            # If feature has -ve values, apply minimal shift to make +ve
            if np.min(v_1_2) <= 0:
                v_1_2 = v_1_2 - np.min(v_1_2) + 0.1
            else:
                pass
            # Find lambda giving 'most-normal' transformed distribution
            lmbda = stats.boxcox_normmax(v_1_2, brack = (-1.9, 2.0),
                                         method = 'mle')
            collect_tf_lmbda.append(lmbda)
            tf = stats.boxcox(v_1_2, lmbda)
            v12_norm_w, v12_norm_p = stats.shapiro(tf)
            # Box-Cox norm plot
            l = stats.boxcox_normplot(v_1_2, -20, 20, plot = axs2[feat_idx],
                                      N = 100)
            axs2[feat_idx].axvline(lmbda, color='r', linestyle='-')
            axs2[feat_idx].set_xlabel('')
            axs2[feat_idx].set_ylabel('')
            axs2[feat_idx].set_title(feat_names[feat_idx], fontsize=4, pad=-15)
            axs2[feat_idx].tick_params(axis="x", pad=-0.5, labelsize=4)
            axs2[feat_idx].tick_params(axis="y", pad=12, labelsize=6)
            axs2[feat_idx].set_ylim([0, 1])
            axs2[feat_idx].set_yticks([0, 1])

            #axs2[feat_idx].text(0.05, 0.70, f'$\lambda$={lmbda:.2f}',
            #                    color='k', fontsize=8,
            #                    transform=axs2[feat_idx].transAxes)

            box_cox_applied[feat_idx] = 1
        else:
            # If we're here, original feature is normal so no transform needed
            collect_tf_lmbda.append(None)
            tf = v_1_2
            v12_norm_w, v12_norm_p = stats.shapiro(tf)
            axs2[feat_idx].set_xticklabels([])
            axs2[feat_idx].tick_params(axis="y", pad=12, labelsize=6)
            #axs2[feat_idx].set_yticklabels([])
            count_orig_normal += 1
            box_cox_applied[feat_idx] = 0

        # qq plot - Box-Cox tranformed if needed
        ff=sm.qqplot(tf, line = 's', ax = axs[feat_idx], fmt = '-k',
                           label = 'V_1_2', lw = 0.5)
        dots = ff.findobj(lambda x: hasattr(x, 'get_color') and
                            x.get_color() == 'r')
        [d.set_color('k') for d in dots]
        [d.set_alpha(0.3) for d in dots]
        [d.set_linestyle('--') for d in dots]
        #axs[feat_idx].set_xticklabels([])
        #axs[feat_idx].set_yticklabels([])
        axs[feat_idx].tick_params(axis="x", pad=-0.5, labelsize=4)
        axs[feat_idx].tick_params(axis="y", labelsize=6)
        axs[feat_idx].set_xlabel('')
        axs[feat_idx].set_ylabel('')
        axs[feat_idx].set_title(feat_names[feat_idx], fontsize = 4, pad = -15)
        axs[feat_idx].tick_params(axis="x", pad=-0.5, labelsize=4)
        axs[feat_idx].tick_params(axis="y", pad=-0.5, labelsize=4)

        # Add CoV of original parameter distribution (v1 and v2)
        #axs[feat_idx].text(0.05, 0.85, f'CoV = {orig_cov:.0f} %',
        #                   color='k',fontsize=8,
        #                   transform=axs[feat_idx].transAxes)

        # qq plot - original feature
        ff_orig=sm.qqplot(v_1_2, line = 's', ax = axs5[feat_idx], fmt = '-k',
                           label = 'V_1_2', lw = 0.5)
        dots = ff_orig.findobj(lambda x: hasattr(x, 'get_color') and
                            x.get_color() == 'r')
        [d.set_color('k') for d in dots]
        [d.set_alpha(0.3) for d in dots]
        [d.set_linestyle('--') for d in dots]
        #axs5[feat_idx].set_xticklabels([])
        #axs5[feat_idx].set_yticklabels([])
        axs5[feat_idx].tick_params(axis="x", pad=-0.5, labelsize=4)
        axs5[feat_idx].tick_params(axis="y", labelsize=6)
        axs5[feat_idx].set_xlabel('')
        axs5[feat_idx].set_ylabel('')
        axs5[feat_idx].set_title(feat_names[feat_idx], fontsize = 4, pad = -15)
        axs5[feat_idx].tick_params(axis="x", pad=-0.5, labelsize=4)
        axs5[feat_idx].tick_params(axis="y", pad=-0.5, labelsize=4)

        # Collect transformed (if needed) values
        tf_reshape = np.reshape(tf, [-1,2], order = 'F')
        feat_vals_v1_tf.append(list(tf_reshape[:,0]))
        feat_vals_v2_tf.append(list(tf_reshape[:,1]))

        # Add plot for histograms of Box-Cox transformed features
        lw_lim = np.min(tf)
        up_lim = np.max(tf)
        mean_val = np.mean(tf)
        std_val = np.std(tf, ddof=1)
        orig_cov = 100*std_val/mean_val
        max_sd = np.max(std_val)
        axs4[feat_idx].hist(tf, bins = 15, range = (lw_lim, up_lim),
                            histtype = 'step', color = 'k',
                            linestyle = 'solid', label = 'V_1_2_boxcox')
        axs4[feat_idx].set_title(feat_names[feat_idx], fontsize=4, pad=-15)
        axs4[feat_idx].tick_params(axis="x", pad=-0.5, labelsize=4)
        axs4[feat_idx].tick_params(axis="y", labelsize=6)
        axs4[feat_idx].set_yticks([0, 75, 150])
        axs4[feat_idx].set_ylim([0,200])
        axs4[feat_idx].set_xlim([mean_val - 5*max_sd, mean_val + 5*max_sd])
        if v12_norm_p >= pthresh:
            #axs4[feat_idx].text(0.05, 0.70, f'p = {v12_norm_p:.4f}', color='k',
            #                  fontsize=8, transform=axs[feat_idx].transAxes)
            count_post_transform_total_normal += 1
        if v12_norm_p < pthresh:
            axs4[feat_idx].text(0.05, 0.70, f'*', color='b',
                              fontsize=14, transform=axs4[feat_idx].transAxes)
            axs[feat_idx].text(0.05, 0.70, f'*', color='b',
                              fontsize=14, transform=axs[feat_idx].transAxes)
            axs[feat_idx].text(0.05, 0.70, f'*', color='b',
                               fontsize=14, transform=axs[feat_idx].transAxes)

        # Add plot for Bland-Altman of Box-Cox transformed features
        diff_tf_1_2 = tf_reshape[:,1] - tf_reshape[:,0]
        diff_mean = np.mean(diff_tf_1_2)
        diff_std = np.std(diff_tf_1_2, ddof=1)
        val_mean = np.mean(tf_reshape, 1)
        axs6[feat_idx].plot(val_mean, diff_tf_1_2, 'k.', markersize = 2)
        axs6[feat_idx].plot([val_mean.min(), val_mean.max()],
                            [diff_mean, diff_mean],
                  'r--',linewidth = 0.5)
        axs6[feat_idx].plot([val_mean.min(), val_mean.max()],
                            [diff_mean - 1.96*diff_std, diff_mean - 1.96*diff_std],
                  'k--',linewidth = 0.5)
        axs6[feat_idx].plot([val_mean.min(), val_mean.max()],
                            [diff_mean + 1.96*diff_std, diff_mean + 1.96*diff_std],
                  'k--',linewidth = 0.5)
        #axs6[feat_idx].grid(axis = 'y', linewidth = 0.5)
        axs6[feat_idx].tick_params(axis="x", pad=-0.5, labelsize=4)
        axs6[feat_idx].tick_params(axis="y", labelsize=4)
        ylimits = axs6[feat_idx].get_ylim()
        ylimits_increase = np.array(ylimits) + [0, np.abs(ylimits[1]*0.4)]
        xlimits = axs6[feat_idx].get_xlim()
        xlimits_increase = np.array(xlimits) + [-np.abs(xlimits[0])*0.1,
                                                +np.abs(xlimits[1])*0.1]
        axs6[feat_idx].set_ylim(ylimits_increase)
        axs6[feat_idx].set_xlim(xlimits_increase)
        #axs6[feat_idx].set_ylim([axs6[feat_idx].get_ylim()[0],
        #                         axs6[feat_idx].get_ylim()[1]*1.4])
        #axs6[feat_idx].set_xlim([axs6[feat_idx].get_xlim()[0]*0.9,
        #                         axs6[feat_idx].get_xlim()[1]*1.1])
        #axs6[feat_idx].set_ylabel("Visit 2 - Visit 1")
        #axs6[feat_idx].set_xlabel("Mean")
        axs6[feat_idx].set_title(feat_names[feat_idx], fontsize=4, pad=-15)
        # Add RC
        rc_result = radfun.icc(tf_reshape,'rc')
        rc_val = helpers.to_precision(rc_result[0],2)
        rc_ll = helpers.to_precision(rc_result[1][0],2)
        rc_ul = helpers.to_precision(rc_result[1][1],2)
        axs6[feat_idx].text(0.02, 0.84, f'{rc_val} ({rc_ll}, {rc_ul})',
                            color='k', fontsize=6,
                            transform=axs6[feat_idx].transAxes)

    for i in range(feat_vals_1.shape[1],10*11):
        plt.figure('qq_boxcox')
        axs[i].axis("off")
        figfuns.figresize()
        plt.subplots_adjust(left=0.027, right=0.990, top=0.990, bottom=0.030,
                            wspace=0.25,hspace=0.25)

        plt.figure('qq_orig')
        axs5[i].axis("off")
        figfuns.figresize()
        plt.subplots_adjust(left=0.027, right=0.990, top=0.990, bottom=0.030,
                            wspace=0.25,hspace=0.25)

        plt.figure('boxcoxnorm')
        axs2[i].axis("off")
        figfuns.figresize()
        plt.subplots_adjust(left=0.027, right=0.990, top=0.990, bottom=0.030,
                            wspace=0.25,hspace=0.25)

        plt.figure('orighist')
        axs3[i].axis("off")
        axs3[i].apply_aspect()
        figfuns.figresize()
        plt.subplots_adjust(left=0.027, right=0.990, top=0.990, bottom=0.030,
                            wspace=0.25,hspace=0.25)

        plt.figure('boxcoxhist')
        axs4[i].axis("off")
        axs4[i].apply_aspect()
        figfuns.figresize()
        plt.subplots_adjust(left=0.027, right=0.990, top=0.990, bottom=0.030,
                            wspace=0.25,hspace=0.25)

        plt.figure('bland-altman')
        axs6[i].axis("off")
        axs6[i].apply_aspect()
        figfuns.figresize()
        plt.subplots_adjust(left=0.027, right=0.990, top=0.990, bottom=0.030,
                            wspace=0.25,hspace=0.25)

    # Save if needed
    if save_figs == 'yes':
        plt.figure('qq_boxcox')
        plt.savefig(os.path.join(save_path,
                                 'qq_105features_poolV1V2_boxcox_transform_ifneeded.pdf'))
        plt.figure('qq_orig')
        plt.savefig(os.path.join(save_path,
                                 'qq_105features_poolV1V2_original.pdf'))
        plt.figure('boxcoxnorm')
        plt.savefig(os.path.join(save_path,'boxcox_normplot.pdf'))
        plt.figure('orighist')
        plt.savefig(os.path.join(save_path,'hists_105features_pool_v1_v2.pdf'))
        plt.figure('boxcoxhist')
        plt.savefig(os.path.join(save_path,'hists_BoxCox_105features_pool_v1_v2.pdf'))
        plt.figure('bland-altman')
        plt.savefig(os.path.join(save_path,'bland-altman_BoxCox_105features.pdf'))

    #%% Calculate intraclass correlation coefficients
    if transform_type == 'none':
        pass
    elif transform_type == 'boxcox':
        tf1 = np.array(feat_vals_v1_tf)
        feat_vals_v1 = np.transpose(tf1).tolist()
        tf2 = np.array(feat_vals_v2_tf)
        feat_vals_v2 = np.transpose(tf2).tolist()
    else:
        sys.exit('!!! invalid option for transform_type')

    for icc_type in icc_type_list:
        icc_vals, icc_cis = radfun.calc_all_icc(feat_vals_v1, feat_vals_v2,
                                                icc_type, feat_class_inds,
                                                feat_names, save_path,
                                                icc_conf_level)
        icc_vals_cis = np.column_stack((icc_vals,icc_cis))
        if save_icc_vals == 'yes':
            #icc_output_dir = (os.path.split(radiomics_dir)[0] +
            #                  '/icc_vals_transform_' + transform_type + '/')
            icc_output_dir = (radiomics_dir +
                              '/icc_vals_transform_' + transform_type + '/')
            os.makedirs(icc_output_dir, exist_ok=True)
            icc_vals_file = ( icc_type + '_T' + radiomics_dir.split('T')[1] +
                              '_' + rois_to_use + '.txt' )
            with open(icc_output_dir + icc_vals_file, 'w') as f:
                np.savetxt(f, icc_vals_cis, fmt="%.16f")
        elif save_icc_vals == 'no':
            pass
        else:
            sys.exit('!!! invalid option for save_icc_vals')

    #%% Bland-Altman for specific features
    # collect_rep_vals = []
    # for f in bland_altman_feature_of_interest:
    #     rep_vals = radfun.bland_altman(feat_vals_v1, feat_vals_v2, feat_names,
    #                                     f, save_path)
    #     collect_rep_vals.append(rep_vals)

    # Print information about number of features originally normal, originally
    # non-normal where transform makes it normal, and originally non-normal
    # where transform doesn't make it normal
    transform_works = count_post_transform_total_normal - count_orig_normal
    transform_fails = len(rad_val) - count_post_transform_total_normal
    print(f'Originally normal:{count_orig_normal}')
    print(f'Originally non-normal, transform works: {transform_works}')
    print(f'Originally non-normal, transform fails: {transform_fails}')

    #%% Temp output
    #return corr_mat
    return feat_vals_v1, feat_vals_v2, box_cox_applied#, corr_mat, pval_mat
