#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Module for run_radiomics function to run travastin radiomics analysis.

Created on Tue Dec  3 08:39:38 2019

@author: damienmchugh
"""
import glob
import os
import re
import sys
from datetime import datetime

import nibabel as nib
from numbers import Number
import numpy as np
import pandas as pd
from radiomics import featureextractor
import SimpleITK as sitk
import six

import helpers
from image_io.jim_io import read_jim_roi

def run_radiomics(data_dir, pt_wldcrd, visit_wldcrd, ims,
                  run_mode,
                  params_dir, params_file,
                  master_spreadsheet_dir, master_spreadsheet,
                  num_roi_col_hdr, pt_inc_col_hdr, pt_id_col_hdr,
                  msk_file_suffix,
                  output_dir, output_file, tmp_exclude = None):
    """Main function for running radiomics analysis.

    Inputs:
        data_dir - path to data
        pt_wldcrd - wildcard for patient directory names
        visit_wldcrd - wildcard for visit directory names
        ims - string specifying which input images to analyse
        run_mode - string specifying to run through and get roi info only, or
                   run full radiomics analysis
        params_dir - path to directory containing yaml files
        params_file - yaml file to use for pyradiomics
        master_spreadsheet_dir - path to directory containing excel files
        master_spreadsheet - excel file from which to take roi/patient info
                           - used for checking number of rois, and which
                             patients to include/exclude
        num_roi_col_hdr - excel column header for roi numbers
        pt_inc_col_hdr - excel column header for patients to include
        pt_id_col_hdr - excel column header for patient ids
        msk_file_suffix - suffix to use for mask files generated from roi files
        output_dir - path to directory in which output directory will be made
        output_file - name of output csv file containing radiomics results
        tmp_exclude - temporary additional argument so one dataset can be
                      excluded due to missing roi

    Output:
        no output returned, but roi info printed to console, or radiomics
        results saved depending on run_mode

    """

    # Use excel file for data inclusion
    xls_pth = master_spreadsheet_dir + master_spreadsheet
    pts_to_exclude = []
    xls_pt_inc = helpers.excel_col_row(xls_pth, 'col', pt_inc_col_hdr)
    xls_pt_ids = helpers.excel_col_row(xls_pth, 'col', pt_id_col_hdr)
    for idx, cell_val in enumerate(xls_pt_inc):
        if not isinstance(cell_val, Number):
            pts_to_exclude.append(xls_pt_ids[idx])

    # Index of visit 1 and visit 2 columns
    # df = pd.read_excel(xls_pth)
    # v1_col = np.where(df.columns=='visit 1')[0][0]
    # v2_col = np.where(df.columns=='visit 2')[0][0]

    # Get expected number of roi files from excel
    # Need to treat T1_postcon and T1_map differently, as these are a subset
    # of the number used for T1_precon and T2
    if ims == 'T1_postcon' or ims == 'T1_map' or ims == 'T1_precon_subset' \
        or ims == 'T2_subset':
        df = pd.read_excel(xls_pth, header = 2)
        get_rows = df[df.iloc[:, 0].str.contains("P", na = False)]
        df_t1post = get_rows.filter(regex = 'T1WPC|Patient ID')

        # Check visits 1 and 2 match
        df_t1post_v1 = df_t1post.iloc[:, 1:6]
        df_t1post_v2 = df_t1post.iloc[:, 6:11]
        assert len(df_t1post_v1.columns) == len(df_t1post_v2.columns), \
            "Number of columns in Visit 1 and 2 do not match"
        for i in range(len(df_t1post_v1.columns)):
            a = df_t1post_v1[df_t1post_v1.iloc[:,i] == 'T1W'].index.tolist()
            b = df_t1post_v2[df_t1post_v2.iloc[:,i] == 'T1W'].index.tolist()
            if a != b:
                print(f'Visit 1 and 2 ROIs inconsistent for T1WPC, lesion {i+1}')

        # 'T1W' in cell indicates T1W ROI is OK for T1WPC
        exp_num_rois = list((df_t1post_v1 == 'T1W').sum(axis = 1))

        # Collect indices for subset of ROIs to use
        roi_subset = []
        for i in range(len(df_t1post.index)):
            x = (df_t1post_v1.iloc[i,:]=='T1W').tolist()
            roi_subset.append(list(np.where(x)[0]))

        # Check above calculations are consistent
        cnt = [len(i) for i in roi_subset]
        assert cnt == exp_num_rois, "Inconsistency in ROI counting"

        # Create dictionary to link ID with ROI subset
        t1wpc_roi_dict = {}
        for i in range(len(roi_subset)):
            t1wpc_roi_dict[df_t1post.iloc[i,0]] = roi_subset[i]

    else:
        exp_num_rois = helpers.excel_col_row(xls_pth,'col',num_roi_col_hdr)

    # Link sequences and ROI file regex
    # N.B. ROI files must have a number and a '.roi' extension
    if ims == 'T2' or ims == 'T2_subset':
        img_roi_dict = {'T2W': 'T2W_joc[0-9]\.roi$'}
    elif ims == 'T2_histmatch':
        img_roi_dict = {'T2W_histmatch': 'T2W_joc[0-9]\.roi$'}
    elif ims == 'T1_precon' or ims == 'T1_precon_subset':
        img_roi_dict = {'T1W_precon': 'T1W_joc[0-9]\.roi$'}
    elif ims == 'T1_precon_histmatch':
        img_roi_dict = {'T1W_precon_histmatch': 'T1W_joc[0-9]\.roi$'}
    elif ims == 'T1_postcon':
        # Subset of ROIs used for T1_precon apply to T1_postcon
        img_roi_dict = {'T1W_postcon': 'T1W_joc[0-9]\.roi$'}
    elif ims == 'T1_map':
        # Same subset of ROIs for T1_postcon apply here
        img_roi_dict = {'T1.raw': 'T1W_joc[0-9]\.roi$'}
    else:
        sys.exit('Unrecognised option for ims')

    # Set up pyradiomics and output directory if needed
    if run_mode == 'radiomics':
        extractor = ( featureextractor.RadiomicsFeatureExtractor(
            os.path.join(params_dir, params_file)) )
        # Create output Analysis directory if it doesn't exist
        param_file_name = os.path.splitext(params_file)[0]
        analysis_dir = ( os.path.join(output_dir,'Analysis_' +
            datetime.now().strftime("%Y-%m-%d-%H-%M-%S") + '_' +
            ims + '_' + param_file_name) )
        os.makedirs(analysis_dir)

    # Loop over patients, visits, scans and apply ROIs
    roi_total_count = 0
    for pt in sorted(glob.glob(os.path.join(data_dir, pt_wldcrd))):
        pt_path = os.path.join(data_dir, pt)
        pt_id = os.path.split(pt)[-1]
        if pt_id in pts_to_exclude:
            continue

        print('********************')
        print(pt_id)
        pt_idx = [i for i, elem in enumerate(xls_pt_ids) if pt_id in elem]
        for visit in sorted(glob.glob(os.path.join(pt_path, visit_wldcrd))):
            visit_path = os.path.join(pt_path,visit)
            visit_name = os.path.split(visit_path)[-1]
            if visit_name == 'visit1' or visit_name == 'visit2':
                visit_num = re.findall(r'\d+', visit_name)[0]
                print('----')
                print('Visit ' + visit_num)
                for scan in list(img_roi_dict):
                    print(scan)

                    # Show ROI info whichever mode we're in
                    if run_mode in ('radiomics','roi_info'):

                        # Get roi files to use
                        roi_files = helpers.roi_file_finder(visit_path,
                                                            img_roi_dict[scan])
                        roi_files = sorted(roi_files)

                        # Get subset of ROIs for T1_postcon and T1_map
                        if ims == 'T1_postcon' or ims == 'T1_map' or \
                            ims == 'T1_precon_subset' or ims == 'T2_subset':
                            # Use dict linking ID to roi subset
                            roi_inds = np.array(t1wpc_roi_dict[pt_id])
                            # Match 0 indexing with file name numbering
                            roi_inds = roi_inds + 1
                            assert len(roi_inds) <= len(roi_files), \
                                "ROI subset is larger than full ROI list"
                            # Only use subset of ROIs
                            roi_files_subset = []
                            for i in roi_files:
                                roinum = int(
                                    re.findall(r'(\d+)\.',
                                               os.path.split(i)[-1])[0])
                                if roinum in roi_inds:
                                    roi_files_subset.append(i)

                            roi_files = roi_files_subset

                        # Check number of roi files matches expected number
                        if len(roi_files) != exp_num_rois[pt_idx[0]]:
                            print('!!! Number of ROI files found does not '
                                  ' match the expected number')
                            print('Found: ' + str(len(roi_files)))
                            print(roi_files)
                            print('Expected: ' +
                                  str(exp_num_rois[pt_idx[0]]))
                            input("Press Enter to continue...")

                    else:
                        sys.exit('run_mode must be \'radiomics\' or '
                                 ' \'roi_info\'')

                    # Loop over ROIs and extract features
                    for roi_file in roi_files:
                        roi_name = os.path.split(roi_file)[-1]
                        print(roi_name)
                        roi_total_count += 1

                        if run_mode == 'radiomics':

                            # Load image
                            img_file = scan + '.img'
                            if ims == 'T1_map':
                                fa_path_a = os.path.join(visit_path,
                                                         'FA_1.img')
                                fa_path_b = os.path.join(visit_path,
                                                         'FA_1_Mean.img')
                                if os.path.exists(fa_path_a):
                                    fa_path = fa_path_a
                                elif os.path.exists(fa_path_b):
                                    fa_path = fa_path_b
                                else:
                                    sys.exit(f'{pt_id}_v{visit_num} - '
                                             'FA file not found')

                                tumour_dir = sorted(glob.glob(
                                    os.path.join(visit_path,'FA_1/tumour*')))
                                img_path_tmp = os.path.join(
                                    tumour_dir[0],
                                    'Tofts_plus_vp_assumed_AIF_results',
                                    img_file)

                                # Need to fix header as T1.raw.img voxel size
                                # is incorrect
                                t1_ana = nib.load(img_path_tmp)
                                fa_ana = nib.load(fa_path)

                                # Create new T1 map with voxdims from FA_1
                                new_header = t1_ana.header
                                new_header.set_zooms(fa_ana.header.get_zooms())
                                t1_new = nib.Spm2AnalyzeImage(t1_ana.dataobj,
                                                              fa_ana.affine,
                                                              new_header)
                                img_file = 'T1_map_header_fixed.img'
                                t1_new_path = os.path.join(visit_path,
                                                           img_file)
                                nib.save(t1_new, t1_new_path)
                            else:
                                pass

                            img_path = os.path.join(visit_path,img_file)
                            img = sitk.ReadImage(img_path)

                            # For T1_precon_subset, base mask on T1_map mask,
                            # so these can be compared directly
                            # T1_map mask is 128x128, need to make this 256x256
                            # to apply to T1_precon_subset
                            if ims == 'T1_precon_subset':
                                # Existing mask is for T1_map
                                msk_file = (os.path.splitext(roi_name)[0] +
                                            '_for_' + 'T1_map' + msk_file_suffix)
                                orig_msk_path = os.path.join(visit_path,
                                                             msk_file)
                                msk_file = msk_file.replace('T1_map',
                                                          'T1_precon_subset')
                                msk_path = os.path.join(visit_path,msk_file)
                                helpers.fix_mask(img_path, orig_msk_path,
                                                 msk_path)

                            else:
                                # Generate mask from roi and save as analyze
                                # Inherit header directly from img
                                im_dim = img.GetSize()
                                vox_size = img.GetSpacing()
                                [m,msk_info] = read_jim_roi(roi_file, im_dim,
                                                            vox_size)
                                m = np.rot90(m,-1)
                                img_ana = nib.load(img_path)
                                msk_ana = nib.Spm2AnalyzeImage(m,
                                                               img_ana.affine,
                                                               img_ana.header)

                                msk_file = (os.path.splitext(roi_name)[0] +
                                            '_for_' + ims + msk_file_suffix)
                                msk_path = os.path.join(visit_path, msk_file)
                                nib.save(msk_ana, msk_path)

                            # Display image and mask
                            # img_array = helpers.sitk_analyze_to_array(img)
                            # figfuns.mont(img_array)
                            # msk = sitk.ReadImage(msk_path)
                            # msk_array = helpers.sitk_analyze_to_array(msk)
                            # figfuns.mont(msk_array)
                            # figfuns.cab()

                            # Calculate features
                            radiomics_dict = extractor.execute(img_path,
                                                               msk_path)
                            # ordered dict -> dict
                            radiomics_dict = dict(radiomics_dict)
                            for key, val in six.iteritems(radiomics_dict):
                                print("\t%s: %s" %(key, val))

                            # Create output subdirectory and write results
                            msk_file_name = os.path.splitext(msk_file)[0]
                            if ims == 'T1_map':
                                scan_name = os.path.splitext(img_file)[0]
                                roidir = '/'.join([pt_id, visit_num, scan_name,
                                                   msk_file_name])
                            else:
                                roidir = '/'.join([pt_id, visit_num, scan,
                                                   msk_file_name])
                            output_dir = os.path.join(analysis_dir, roidir)
                            os.makedirs(output_dir)
                            helpers.write_dict_to_csv(radiomics_dict,
                                                      output_dir, output_file)

                    # img_load = nib.load(os.path.join(visit_path,img_file))
                    # roi_load = nib.load(os.path.join(visit_path,roi_file))
                    # img = np.rot90(np.flipud(np.squeeze(img_load.get_fdata())),-1)
                    # roi = np.rot90(np.flipud(np.squeeze(roi_load.get_fdata())),-1)
                    # msk = roi>0
                    # figfuns.mont(img)
                    # figfuns.mont(roi)
                    # figfuns.mont(msk)
                    # # Check size of image and msk
                    # assert(np.shape(img) == np.shape(msk)), \
                    #     "Image and mask dimensions are not equal"
    print('********************')
    print(f'Total number of rois: {roi_total_count}')
